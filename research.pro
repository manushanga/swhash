#-------------------------------------------------
#
# Project created by QtCreator 2014-08-27T18:24:57
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = research
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    naive_bsort.cpp \
    naive.cpp \
    alphabet.cpp \
    filereader.cpp \
    debug.cpp \
    qcustomplot.cpp

HEADERS  += mainwindow.h \
    alphabet.h \
    debug.h \
    filereader.h \
    naive.h \
    naive_bsort.h \
    sequence.h \
    testfunc.h \
    qcustomplot.h \
    graph.h

FORMS    += mainwindow.ui
QMAKE_CXXFLAGS += -std=c++11
DEFINES += QT

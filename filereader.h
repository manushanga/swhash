#include <string>
#include <cstdio>
#include <cstring>
#include <vector>

#include "sequence.h"
class FileReader
{
private:
	uint8_t *mdata;
	uint64_t mtotal,*mboundaries,mboundary_count;
	std::vector< SASSequence > sequences_;
	std::string mfilename;
public:
	FileReader(std::string filename);
	uint64_t GetSequence(uint64_t offset);
	void GetData(uint8_t** data, uint64_t** boundaries, uint64_t* total, uint64_t *total_boundaries);
    void GetData2Bits( std::vector< SASSequence >& sequences, uint64_t* total, uint64_t* boundary_count );
	void DumpData(std::string filename);
	~FileReader();
};

#ifndef __SEQ
#define __SEQ
#include <vector>

struct SASRegion {
	uint64_t start;
	uint64_t end;
};
struct SASResultSequence {
	uint8_t *hits;
};
struct SASSequence {
	uint64_t *sequence;
	uint64_t sequence_length;
	std::vector< SASRegion > regions;
};
struct SASUniqueSequenceIterator{
	uint64_t index;
	SASUniqueSequenceIterator(){ index=0UL; }
};
struct SASUniqueSequence{
	uint64_t start,end;
	SASUniqueSequence() {start=0UL; end=0UL;}
};

#endif
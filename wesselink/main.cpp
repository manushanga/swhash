#include <iostream>
#include <cmath>
#include <unordered_map>
#include <chrono>
using namespace std;
#define HASH_SIZE 815669
#define PRIMES_SIZE 30
std::string filename;
uint32_t primes[PRIMES_SIZE];
char map[]={'$','X','a','c','g','t'};
uint32_t *output;
uint8_t *data;
uint32_t data_total;
struct SubSequence {
	uint32_t start,end,seen;
};

SubSequence hash_table[HASH_SIZE];

unsigned char mask[]={0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02, 0x01};
#define tget(i) ( (t[(i)/8]&mask[(i)%8]) ? 1 : 0 )
#define tset(i, b) t[(i)/8]=(b) ? (mask[(i)%8]|t[(i)/8]) : ((~mask[(i)%8])&t[(i)/8])
#define chr(i) (cs==sizeof(int)?((int*)s)[i]:((unsigned char *)s)[i])
#define isLMS(i) (i>0 && tget(i) && !tget(i-1))
// find the start or end of each bucket
void getBuckets(unsigned char *s, int *bkt, int n, int K, int cs, bool end) {
int i, sum=0;
for(i=0; i<=K; i++) bkt[i]=0; // clear all buckets
for(i=0; i<n; i++) bkt[chr(i)]++; // compute the size of each bucket
for(i=0; i<=K; i++) { sum+=bkt[i]; bkt[i]=end ? sum : sum-bkt[i]; }
}
// compute SAl
void induceSAl(unsigned char *t, int *SA, unsigned char *s, int *bkt,
int n, int K, int cs, bool end) {
int i, j;
getBuckets(s, bkt, n, K, cs, end); // find starts of buckets
for(i=0; i<n; i++) {
j=SA[i]-1;
if(j>=0 && !tget(j)) SA[bkt[chr(j)]++]=j;
}
}
// compute SAs
void induceSAs(unsigned char *t, int *SA, unsigned char *s, int *bkt,
int n, int K, int cs, bool end) {
int i, j;
getBuckets(s, bkt, n, K, cs, end); // find ends of buckets
for(i=n-1; i>=0; i--) {
j=SA[i]-1;
if(j>=0 && tget(j)) SA[--bkt[chr(j)]]=j;
}
}
// find the suffix array SA of s[0..n-1] in {1..K}^n
// require s[n-1]=0 (the sentinel!), n>=2
// use a working space (excluding s and SA) of at most 2.25n+O(1) for a constant alphabet
void SA_IS(unsigned char *s, int *SA, int n, int K, int cs) {
int i, j;
unsigned char *t=(unsigned char *)malloc(n/8+1); // LS-type array in bits
// Classify the type of each character
tset(n-2, 0); tset(n-1, 1); // the sentinel must be in s1, important!!!
for(i=n-3; i>=0; i--)
tset(i, (chr(i)<chr(i+1) || (chr(i)==chr(i+1) && tget(i+1)==1))?1:0);
// stage 1: reduce the problem by at least 1/2
// sort all the S-substrings
int *bkt = (int *)malloc(sizeof(int)*(K+1)); // bucket array
getBuckets(s, bkt, n, K, cs, true); // find ends of buckets
for(i=0; i<n; i++) SA[i]=-1;
for(i=1; i<n; i++)
if(isLMS(i)) SA[--bkt[chr(i)]]=i;
induceSAl(t, SA, s, bkt, n, K, cs, false);
induceSAs(t, SA, s, bkt, n, K, cs, true);
free(bkt);
// compact all the sorted substrings into the first n1 items of SA
// 2*n1 must be not larger than n (proveable)
int n1=0;
for(i=0; i<n; i++)
if(isLMS(SA[i])) SA[n1++]=SA[i];
// find the lexicographic names of all substrings
for(i=n1; i<n; i++) SA[i]=-1; // init the name array buffer
int name=0, prev=-1;
for(i=0; i<n1; i++) {
int pos=SA[i]; bool diff=false;
for(int d=0; d<n; d++)
if(prev==-1 || chr(pos+d)!=chr(prev+d) || tget(pos+d)!=tget(prev+d))
{ diff=true; break; }
else if(d>0 && (isLMS(pos+d) || isLMS(prev+d))) break;
if(diff) { name++; prev=pos; }
pos=(pos%2==0)?pos/2:(pos-1)/2;
SA[n1+pos]=name-1;
}
for(i=n-1, j=n-1; i>=n1; i--)
if(SA[i]>=0) SA[j--]=SA[i];
// stage 2: solve the reduced problem
// recurse if names are not yet unique
int *SA1=SA, *s1=SA+n-n1;
if(name<n1)
SA_IS((unsigned char*)s1, SA1, n1, name-1, sizeof(int));
else // generate the suffix array of s1 directly
for(i=0; i<n1; i++) SA1[s1[i]] = i;
// stage 3: induce the result for the original problem
bkt = (int *)malloc(sizeof(int)*(K+1)); // bucket array
// put all left-most S characters into their buckets
getBuckets(s, bkt, n, K, cs, true); // find ends of buckets
for(i=1, j=0; i<n; i++)
if(isLMS(i)) s1[j++]=i; // get p1
for(i=0; i<n1; i++) SA1[i]=s1[SA1[i]]; // get index in s
for(i=n1; i<n; i++) SA[i]=-1; // init SA[n1..n-1]
for(i=n1-1; i>=0; i--) {
j=SA[i]; SA[i]=-1;
SA[--bkt[chr(j)]]=j;
}
induceSAl(t, SA, s, bkt, n, K, cs, false);
induceSAs(t, SA, s, bkt, n, K, cs, true);
free(bkt); free(t);
}


uint32_t ipow(uint32_t n, uint32_t p)
{
	
	uint32_t res = (n*n) % HASH_SIZE;
	while (p>0){
		res *= n;
		res %= HASH_SIZE;
		p--;
	}
	
	return res ;
}
uint32_t hash_func(uint32_t start, uint32_t end)
{
	uint32_t hash=1;
	for (uint32_t i=start;i<=end;i++){
		hash =  (hash * ipow( primes[i-start] , data[i] ))% HASH_SIZE;
	}
	
	
	return hash %HASH_SIZE;
}
bool is_prime(uint32_t n)
{
	uint32_t sq = (uint32_t)floor ( sqrt(n) );
	
	while (sq>1){
		if ( (n%sq) == 0)
			return false;
		sq--;
	}
	return true;
}
void generate_primes(){
	primes[0]=2;
	primes[1]=3;
	uint32_t i=2;
	uint32_t next=5;
	while (i<PRIMES_SIZE)
	{
		if (is_prime(next)){
			primes[i]=next;
			i++;	
		} 
		next++;
	}
}

void hash_table_add(uint32_t start, uint32_t end)
{
	uint32_t hh=hash_func(start,end);
	uint32_t ho=hh;
	
	while (hash_table[hh].start != 0xFFFFFFF)
	{
		bool found=true;
		for (uint32_t i=0;i<=end-start;i++){
			if ( data[hash_table[hh].start + i] != data[start +i] ) {
				
				found=false;
			}
		}
		
		if (found){
			hash_table[hh].seen++;
			return;
		} else {
			hh = ( hh+ hh ) % HASH_SIZE;
			//cout<<hh<<endl;
			if (hh==ho)
			{
				cout<<"no space"<<endl;
				return;
			}
		}
	}
	hash_table[hh].start = start;
	hash_table[hh].end = end;
	hash_table[hh].seen = 1;
}
void print_sequence(uint32_t start, uint32_t end)
{
	for (uint32_t i=start;i<=end;i++)
	{
		cout<<map[data[i]];
	}
	cout<<endl;
}
void list_uniques(uint32_t size)
{
	for (uint32_t i=0;i<data_total-size +1;i++)
	{
		//if (i%10000 == 0)
		//	cout<<"adding"<<i<<endl;
		bool noskip=true;
		for (uint32_t j=i;j<=i+size-1;j++){
			if (data[j]==0 || data[j] ==1){
				noskip=false;
				break;
			}
		}
		if (noskip)
			hash_table_add(i,i+size-1);
	}
	for (uint32_t i=0;i<HASH_SIZE;i++)
	{
		//if (hash_table[i].start != 0xFFFFFFF && hash_table[i].seen == 1)
		//{
		//	print_sequence(hash_table[i].start,hash_table[i].end);
		//}
	}
	
}
void allocate_memory(std::string _filename)
{
	filename = _filename;
	data_total=0;
	FILE *file=fopen(filename.c_str(),"r");
	int state=0;
	
	while (!feof(file)){
		switch (state){
			
			case 0:
			{
				if (fgetc(file)=='>'){
					state=1;
				}
				break;
			}
			case 1: // read the sequence name
			{
				if (fgetc(file)=='\n'){
					state=2;
				}
				break;
			}
			case 2:
			{
				int c=fgetc(file);
				char cc=toupper(c);
				if (c=='>'){
					state=1;
					//data_total++;
				} else if (c=='\n'){
				} else if (cc=='A' || cc=='C' || cc == 'G' || cc == 'T') {
					data_total++;
				} else if (c!=-1) {
					data_total++;
				}
				break;
			}
			
		}
	}
	data_total++;
	fclose(file);
	data = (uint8_t *)malloc(data_total+1);
	output=(uint32_t *)malloc((data_total+1)*sizeof(uint32_t));
}

void deallocate_memory()
{
	free(data);
}
void load_data()
{
	FILE *file=fopen(filename.c_str(),"r");
	int state=0;
	uint8_t rev[256];
	uint32_t write=0;
	
	rev['A']=2;
	rev['C']=3;
	rev['G']=4;
	rev['T']=5;
	
	
	while (!feof(file)){
		switch (state){
			case 0:
			{
				if (fgetc(file)=='>'){
					state=1;
				}
				break;
			}
			case 1: // read the sequence name
			{
				if (fgetc(file)=='\n'){
					state=2;
				}
				break;
			}
			case 2:
			{
				int c=fgetc(file);
				char cc=toupper(c);
				if (c=='>'){
					state=1;
					//data[write]=1;//0;//rev[Alphabet::map[0]];
					//write++;
				} else if (c=='\n'){
				} else if (cc=='A' || cc=='C' || cc == 'G' || cc == 'T') {
					data[write]=rev[cc];
					write++;
				} else if (c!=-1){
					data[write]=1;
					write++;
				}
				break;
			}
		}
	}
	data[write] = 0;
	fclose(file);
}
int main(int argc, char **argv) {
	allocate_memory(argv[1]);
	generate_primes();
	load_data();
	uint32_t sz=PRIMES_SIZE;
	
	auto t1x=std::chrono::high_resolution_clock::now();
	/*while (sz>4){
		for (uint32_t i=0;i<HASH_SIZE;i++)
			hash_table[i].start = 0xFFFFFFF;
		
		list_uniques(sz);
		sz--;
	}*/
	
	SA_IS((unsigned char *) data, (int*)output,data_total,6,1);
	auto t2x=std::chrono::high_resolution_clock::now();
	//while (1) {}
	uint32_t ix=0;
 	for (int i=0;i<data_total;i++){
		ix+=output[i];
		//print_sequence(output[i],output[i]+10);
	}
	std::chrono::duration<double> timespanx=std::chrono::duration_cast<std::chrono::duration<double>>(t2x-t1x);
	std::cout<<ix<<std::endl;
	std::cout << timespanx.count()<< std::endl;
	getchar();
	deallocate_memory();
    return 0;
}

#include "naive_bsort.h"
#include <iostream>
RadixComparatorBSort::RadixComparatorBSort(uint64_t column, uint8_t* input)
: col(column), in(input)
{
}
bool RadixComparatorBSort::operator()(const uint64_t& a, const uint64_t& b)
{
	return in[a+col] < in[b+col];
}

SASFunc* SASNaiveBSort::create()
{
	static SASNaiveBSort naive;
	return &naive;
}
void SASNaiveBSort::swap(uint64_t* a, uint64_t* b)
{
	uint64_t c = *a;
	*a = *b;
	*b = c;
}

SASNaiveBSort::SASNaiveBSort()
{
}

void SASNaiveBSort::BSort(uint8_t *input, uint64_t *output, uint64_t col, uint64_t *endings)
{
	uint64_t write=endings[0];
	uint64_t end=endings[1];
		
	for (uint8_t alpha=0;alpha<(uint8_t) Alphabet::size;alpha++){
		for (uint64_t i=write;i<=end;i++){
			
			if (output[i]+col < total && input[output[i]+col]==alpha){
				SASNaiveBSort::swap(&output[ write ] , &output[i]);
				write++;
			}
		}
		endings[alpha]=write;
	}
	
}

void SASNaiveBSort::RadixRecursive(uint8_t *input, uint64_t current, uint64_t low, uint64_t high, uint64_t *output)
{
	
	if (current < unique_max_length && low < high){
		uint64_t endings[Alphabet::size];
		endings[0]=low;
		endings[1]=high;
		BSort(input,output,current,endings);
// 		
		uint64_t slow=low;
		for (int i=0;i<Alphabet::size;i++){
			
			if (endings[i] > slow){
				//std::cout<<low<<" "<<high<<" "<<slow<<" "<<endings[i]-1<<std::endl;
				RadixRecursive(input, current + 1, slow, endings[i]-1, output);
				
				slow=endings[i];
			}
		}
		
	}
}

void SASNaiveBSort::SASRun(void *input, void *output)
{
	uint8_t *in=(uint8_t*) input;
	uint64_t *out=(uint64_t*) output;
	for (uint64_t i=0;i<total;i++)
	{
		out[i]=i;
	}
	RadixRecursive(in, 0, 0, total-1, out);

}

void SASNaiveBSort::SASNextUniqueSequence(uint8_t* input, uint64_t* output, SASUniqueSequenceIterator* iterator, SASUniqueSequence* sequence)
{
	sequence->start = (uint64_t)-1UL;
	if (input[output[iterator->index]] == ALPHA_SEQ_END ){
		// add 2 to skip the starting $ at the row above starting row
		iterator->index += 1;
	} else if (iterator->index < total-1) {
		// we assume unique_max_length < any sequence length
		uint64_t upperidx=output[iterator->index-1];
		uint64_t idx=output[iterator->index];
		uint64_t belowidx=output[iterator->index+1];
		
		uint64_t upperLCP=0;
		uint64_t belowLCP=0;
		
		uint64_t i;
		uint64_t maxlen=unique_max_length;
		
		for (i=0;i<unique_max_length;i++){
			// we skip here to avoid from overly complex machinery for the corner
			// cases, 99% of unqiues can be taken from the other methods 
			
			if (i+upperidx >= total || i+idx >= total || i+belowidx >= total)
			{
				iterator->index++;
				return;
			}
			if ( (input[i+upperidx] == ALPHA_AMBIGUOUS ) ||
				 (input[i+idx] == ALPHA_AMBIGUOUS ) || 
				 (input[i+belowidx] == ALPHA_AMBIGUOUS )){
				maxlen=i;	
				break;
			} else if (input[i+idx] == ALPHA_SEQ_END ){
				maxlen=i;
				break;
			}
			
			
		}
		
		
		bool loopb1=false,loopb2=false,end1=false,end2=false;
		for (i=0;i<maxlen;i++){

			
			if (input[i+idx] != input[i+upperidx] || input[i+upperidx] == ALPHA_SEQ_END){
				upperLCP=i;
				loopb1=true;
				break;
			}
		}
		for (i=0;i<maxlen;i++){
			
			
			if (input[i+idx] != input[i+belowidx] || input[i+belowidx] == ALPHA_SEQ_END){
				belowLCP=i;
				loopb2=true;
				break;
			}
		}
		
		
		
		
		uint64_t lcp =upperLCP > belowLCP ? upperLCP : belowLCP ;
		if (loopb1 && loopb2 ) {
			if (lcp == 0){
				sequence->start = idx;
				sequence->end = sequence->start + maxlen;
				
			} else {
				//std::cout<<upperLCP <<" "<< belowLCP<<" " <<lcp <<" "<< iterator->index<<std::endl;
				sequence->start = idx;
				sequence->end = sequence->start + lcp ;
				
			}
		} 
		iterator->index++;
		
	}
}

SASNaiveBSort::~SASNaiveBSort()
{

}

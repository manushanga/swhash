#ifndef __H_ALPHA
#define __H_ALPHA
#include <iostream>
#include <iomanip>
#include <stdint.h>

#include "sequence.h"

#define ALPHA_SEQ_END 0
#define ALPHA_AMBIGUOUS 1

struct Alphabet
{
	static char map[];
	static char map_simple[];
	static char map_simple_rev[];
	static const int size=6;
	static const char delemiter=0;
	static void print_input(uint8_t *input, uint64_t n);
	static void print_input_array(uint8_t *input, uint64_t n);    
	static void print_input_simple(uint8_t *input, uint64_t n);
	static void print_output(uint8_t *input, uint64_t *output, uint64_t n, uint64_t until);
	static void print_output(uint8_t *input, uint64_t *output, uint64_t n);
	static void print_output_simple(uint8_t *input, uint64_t *output, uint64_t n);
	static void print_output(uint64_t *output, uint64_t n);
	static void print_sequence(uint8_t *input, SASUniqueSequence *sequence);
	static void print_sequence(FILE *to, uint8_t *input, SASUniqueSequence *sequence);
	static void print_sequence_simple(uint64_t sequence);
	static void print_sequence_2bit(uint64_t seq, int len);
	static uint8_t *generate_input(uint64_t n);
	static uint64_t *generate_output(uint64_t n);
	static void destroy_output(uint64_t *output);
};

#endif
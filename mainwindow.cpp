#include "mainwindow.h"
#include "ui_mainwindow.h"
MainWindow::MainWindow(Graph *graph, DrawType drawType, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setCentralWidget(&plot);
    QFont ff=plot.font();
    ff.setPointSize(13);
    plot.xAxis->setLabelFont(ff);
    plot.xAxis->setTickLabelFont(ff);
    plot.yAxis->setLabelFont(ff);
    plot.yAxis->setTickLabelFont(ff);

    if (drawType == DrawTime)
        drawTime(graph,&plot);
    else if (drawType == DrawMemory)
        drawMemory(graph,&plot);
    else
        drawMemoryPerInput(graph, &plot);

}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::drawTime(Graph *graph, QCustomPlot *customPlot)
{
    setWindowTitle("CPU time");
    QVector<double> ticks;
    QVector<double> values;
    QVector<QString> labels;

    customPlot->xAxis->setLabel("Inputs");
    customPlot->yAxis->setLabel("Time (s)");

    for (int j=0;j<graph->points[0].size();j++)
    {
        QString label = QString(graph->inputs[j].c_str()) +"\n"+QString::number(graph->points[0][j].input_size) + "\n (" + QString::number(graph->points[0][j].avg_size) +")";
        // prepare x axis with country labels:

        ticks << j;
        labels << label;


    }


    customPlot->xAxis->setAutoTicks(false);
    customPlot->xAxis->setAutoTickLabels(false);
    customPlot->xAxis->setTickVector(ticks);
    customPlot->xAxis->setTickVectorLabels(labels);
    customPlot->xAxis->setTickLabelRotation(60);
    customPlot->xAxis->setSubTickCount(0);
    customPlot->xAxis->setTickLength(0, 4);
    customPlot->xAxis->grid()->setVisible(true);
    customPlot->xAxis->setRange(0, labels.size() );
    double max=0;
    for (int i=0;i<graph->points.size();i++)
    {
        customPlot->addGraph();
        QVector<double> values;
        for (int j=0;j<graph->points[i].size();j++)
        {
            if (max<graph->points[i][j].time)
            {
                max=graph->points[i][j].time;
            }
            values.append(graph->points[i][j].time);

        }
        customPlot->graph(i)->setData(ticks, values);
        customPlot->graph(i)->setName(QString(graph->algos[i].c_str()));
        customPlot->graph(i)->setPen(QPen(QColor(graph->colors[i].c_str())));
    }
    customPlot->yAxis->setRange(0,max);

    customPlot->legend->setVisible(true);
    customPlot->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignTop|Qt::AlignHCenter);
    customPlot->legend->setBrush(QColor(255, 255, 255, 200));
    QPen legendPen;
    legendPen.setColor(QColor(130, 130, 130, 200));
    customPlot->legend->setBorderPen(legendPen);
    QFont legendFont = font();
    legendFont.setPointSize(15);
    customPlot->legend->setFont(legendFont);
    customPlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom);

}
void MainWindow::drawMemory(Graph *graph, QCustomPlot *customPlot)
{
    setWindowTitle("Memory usage");
    QVector<double> ticks;
    QVector<double> values;
    QVector<QString> labels;

    customPlot->xAxis->setLabel("Inputs");
    customPlot->yAxis->setLabel("Memory (kB)");

    for (int j=0;j<graph->points[0].size();j++)
    {
        QString label = QString(graph->inputs[j].c_str()) +"\n"+ QString::number(graph->points[0][j].input_size) + "\n (" + QString::number(graph->points[0][j].avg_size) +")";
        // prepare x axis with country labels:

        ticks << j;
        labels << label;


    }


    customPlot->xAxis->setAutoTicks(false);
    customPlot->xAxis->setAutoTickLabels(false);
    customPlot->xAxis->setTickVector(ticks);
    customPlot->xAxis->setTickVectorLabels(labels);
    customPlot->xAxis->setTickLabelRotation(60);
    customPlot->xAxis->setSubTickCount(0);
    customPlot->xAxis->setTickLength(0, 4);
    customPlot->xAxis->grid()->setVisible(true);
    customPlot->xAxis->setRange(0, labels.size() );
    double max=0;
    for (int i=0;i<graph->points.size();i++)
    {
        customPlot->addGraph();
        QVector<double> values;
        for (int j=0;j<graph->points[i].size();j++)
        {
            double xx=graph->points[i][j].memory;
            if (max<xx)
            {
                max=xx;
            }
            values.append(xx);

        }
        customPlot->graph(i)->setData(ticks, values);
        customPlot->graph(i)->setName(QString(graph->algos[i].c_str()));
        customPlot->graph(i)->setPen(QPen(QColor(graph->colors[i].c_str())));
    }
    customPlot->yAxis->setRange(0,max);

    customPlot->legend->setVisible(true);
    customPlot->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignTop|Qt::AlignHCenter);
    customPlot->legend->setBrush(QColor(255, 255, 255, 200));
    QPen legendPen;
    legendPen.setColor(QColor(130, 130, 130, 200));
    customPlot->legend->setBorderPen(legendPen);
    QFont legendFont = font();
    legendFont.setPointSize(15);
    customPlot->legend->setFont(legendFont);
    customPlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom);

}

void MainWindow::drawMemoryPerInput(Graph *graph, QCustomPlot *customPlot)
{
    setWindowTitle("Memory usage / Input size");

    QVector<double> ticks;
    QVector<double> values;
    QVector<QString> labels;

    customPlot->xAxis->setLabel("Inputs");
    customPlot->yAxis->setLabel("Memory (kB)");

    for (int j=0;j<graph->points[0].size();j++)
    {
        QString label = QString(graph->inputs[j].c_str()) +"\n"+ QString::number(graph->points[0][j].input_size) + "\n (" + QString::number(graph->points[0][j].avg_size) +")";
        // prepare x axis with country labels:

        ticks << j;
        labels << label;


    }


    customPlot->xAxis->setAutoTicks(false);
    customPlot->xAxis->setAutoTickLabels(false);
    customPlot->xAxis->setTickVector(ticks);
    customPlot->xAxis->setTickVectorLabels(labels);
    customPlot->xAxis->setTickLabelRotation(60);
    customPlot->xAxis->setSubTickCount(0);
    customPlot->xAxis->setTickLength(0, 4);
    customPlot->xAxis->grid()->setVisible(true);
    customPlot->xAxis->setRange(0, labels.size() );
    double max=0;
    for (int i=0;i<graph->points.size();i++)
    {
        customPlot->addGraph();
        QVector<double> values;
        for (int j=0;j<graph->points[i].size();j++)
        {
            double xx=graph->points[i][j].memory*1024/(graph->points[i][j].input_size*graph->points[i][j].avg_size);
            if (max<xx)
            {
                max=xx;
            }
            values.append(xx);

        }
        customPlot->graph(i)->setData(ticks, values);
        customPlot->graph(i)->setName(QString(graph->algos[i].c_str()));
        customPlot->graph(i)->setPen(QPen(QColor(graph->colors[i].c_str())));
    }
    customPlot->yAxis->setRange(0,max);

    customPlot->legend->setVisible(true);
    customPlot->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignTop|Qt::AlignHCenter);
    customPlot->legend->setBrush(QColor(255, 255, 255, 200));
    QPen legendPen;
    legendPen.setColor(QColor(130, 130, 130, 200));
    customPlot->legend->setBorderPen(legendPen);
    QFont legendFont = font();
    legendFont.setPointSize(15);
    customPlot->legend->setFont(legendFont);
    customPlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom);
}

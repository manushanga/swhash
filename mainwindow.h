#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "graph.h"
#include "qcustomplot.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow( Graph *graph, DrawType drawType, QWidget *parent = 0);
    ~MainWindow();

private:
    QCustomPlot plot;
    Ui::MainWindow *ui;
    void drawTime(Graph *graph, QCustomPlot *customPlot);
    void drawMemory(Graph *graph, QCustomPlot *customPlot);
    void drawMemoryPerInput(Graph *graph, QCustomPlot *customPlot);
};

#endif // MAINWINDOW_H

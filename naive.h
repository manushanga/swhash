#include <stdint.h>
#include <algorithm>

#include "testfunc.h"

struct RadixComparator
{
	uint64_t col;
	uint8_t *in;
	RadixComparator(uint64_t column, uint8_t* input);
	bool operator()(const uint64_t &a, const uint64_t &b);
};
class SASNaive : SASFunc
{
private:
	char *in;
	uint64_t *out;
public:

	static SASFunc *create();
	SASNaive();
	void RadixRecursive(uint8_t* input, uint64_t current, uint64_t low, uint64_t high, uint64_t* output);
	virtual void SASRun(void* input, void* output);
    virtual void SASNextUniqueSequence(uint8_t* input, uint64_t* output, SASUniqueSequenceIterator* iterator, SASUniqueSequence* sequence);
	virtual ~SASNaive();
};
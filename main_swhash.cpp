#include <iostream>
#include <vector>
#include <chrono>
#include <cmath>
#include <queue>
#include "filereader.h"
#include "alphabet.h"
#include "debug.h"
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <errno.h>
#include <unistd.h>
#include <cstdio>
#include <cstdlib>
#include <fcntl.h>
/* GCC intrinsic headers */
#include <cpuid.h>
#include <smmintrin.h>
#include <immintrin.h>
//#include <wmmintrin.h>
#include <bitset>
#define GET_SEQ_PRIMER(bases,i) (i&31)==0?  bases[i>>5] :  (bases[i>>5]<<(2*(i&31))) | (bases[(i>>5) + 1] >> (64-2*(i&31)))

#define GET_SEQ64_BASE(bases,i) ( (bases<<(2*i)) >> 62 )
#define GET_SEQ64_BASE_CHAR(bases,i) Alphabet::map_simple[ ((bases<<(2*i)) >> 62 ) ]

#define GET_SEQ_BASE(bases,i) ( (bases[i/32]<<(2*i)) >> 62 )
#define GET_SEQ_BASE_CHAR(bases,i) Alphabet::map_simple[ ( ((bases[i/32]<<(2*i)) >> 62 ) ]

#define HITMAP_SZ 2097152 // 4^12/8 = 2097152 
#define BLOOM_MIN_SZ (0x01L << 20)
#define PRIMER_MAX_SZ 32
#define PRIMER_MIN_SZ 4
#define PRIMER_MIN_MTEMP 50
#define PRIMER_MAX_MTEMP 65
#define PRIMER_MAX_REPEAT_SZ 4
#define TARGET_MAX_SZ 600
#define TARGET_MIN_SZ 100
#define SEQ_MIN_PRIMERS 10
#define SEQ_MAX_PRIMERS 200


#define PRIMER3_TEXT_SZ 150+32+32+TARGET_MAX_SZ+32+32
#define PRIMER3_PATH "/home/madura/wrk/research/primer3-2.3.6/src/"
#define PRIMER3_NAME "primer3_core"
#define PRIMER3_MAX_PICK_SZ 3
#define PRIMER3_MAX_TEMPDIFF 20


const uint64_t SLIDE_MASK=0xFFFFFFFFFFFFFFFFUL;

std::vector< SASSequence > sequences;
std::vector< SASResultSequence > results;
uint64_t total,boundaries;
std::vector<int> matches;
uint64_t tot=0,tot_sl=0;
FILE *foutput=NULL;
struct MatchMeta {
    uint64_t position;
    int tm;
    MatchMeta(uint64_t pos_, int tm_) : position(pos_), tm(tm_) {}
};
typedef uint64_t (*hash_func)(uint64_t m);

struct ArgumentOption {
    std::string short_switch;
    std::string desc;
    std::string data;
    int data_count;
    bool present;
};

struct Bitmap {
    uint64_t total;
    uint8_t *sequence;
};
std::vector<Bitmap> bitmaps;

enum options_t {OPT_INPUT=0,OPT_OUTPUT,OPT_OUTTYPE,OPT_BITMAP};
ArgumentOption options[] =
{
    {"-i","input file with sequences","",1,false},
    {"-o","output file","",1,false},
    {"-ot","output type: positions, Primer3 (runs uniques through Primer3) or uniques","",1,false},
    {"-m","output a binary map file, takes an argument as filename","",1,false}
};

uint64_t MURMUR(uint64_t m)
{
    uint64_t mL= (m <<32) >>32;
    uint64_t mH= (m >>32);
    mL ^= mL >> 16;
    mL *= 0x85ebca6b;
    mL ^= mL >> 13;
    mL *= 0xc2b2ae35;
    mL ^= mL >> 16;

    mH ^= mH >> 16;
    mH *= 0x85ebca6b;
    mH ^= mH >> 13;
    mH *= 0xc2b2ae35;
    mH ^= mH >> 16;

    return mL | mH <<32;
}

uint64_t CRC(uint64_t m)
{
    return _mm_crc32_u64(0,m);
}
uint64_t FNV(uint64_t m)
{
    uint64_t hash=0xcbf29ce485222325L;
    hash=hash * 0x100000001b3;
    hash=hash ^ m & 0x00000000000000FF;

    hash=hash * 0x100000001b3;
    hash=hash ^ (m & 0x000000000000FF00)>>8;

    hash=hash * 0x100000001b3;
    hash=hash ^ (m & 0x0000000000FF0000)>>16;

    hash=hash * 0x100000001b3;
    hash=hash ^ (m & 0x00000000FF000000)>>24;

    hash=hash * 0x100000001b3;
    hash=hash ^ (m & 0x000000FF00000000)>>32;

    hash=hash * 0x100000001b3;
    hash=hash ^ (m & 0x0000FF0000000000)>>40;

    hash=hash * 0x100000001b3;
    hash=hash ^ (m & 0x00FF000000000000)>>48;

    hash=hash * 0x100000001b3;
    hash=hash ^ (m & 0xFF00000000000000)>>56;

    return hash;
}
inline bool evaluate(uint64_t match,
                     int length, int *tm)
{
    int base_counts[4]= {0};
    int base_count_consec=0;
    int max_norepeat_length=0;
    int previous=255;
    for (int i=0; i<length; i++)
    {
        int base=GET_SEQ64_BASE(match,i);
        if (previous==base)
            base_count_consec++;
        else
            base_count_consec=1;
        base_counts[base]++;
        //std::cout<<GET_SEQ64_BASE_CHAR(match,i)<<std::endl;
        if (base_count_consec>PRIMER_MAX_REPEAT_SZ)
        {
            return false;
        }
        previous =base;
    }
    // A,c,G,T
    float gc=base_counts[1]+base_counts[2];
    float acgt=base_counts[0]+base_counts[1]+base_counts[2]+base_counts[3];
    /*Alphabet::print_sequence_simple(match);
    std::cout<<gc<<" "<<acgt<<std::endl;
    */
    *tm = gc*4 + (acgt-gc)*2;
    return (gc > 0.4*acgt && gc< 0.6*acgt);

}
char *write_sequence32_rev_primer(char *write_to, uint64_t sequence, int n)
{
    for (int i=62-2*n; i<64; i+=2)
    {
        *write_to = (char) tolower(Alphabet::map_simple_rev [ ((( uint64_t(0x03) <<i) & sequence) >> i) ] );
        write_to++;
    }
    return write_to;
}
char *write_sequence32(char *write_to, uint64_t sequence, int n)
{
    for (int i=62; i>=62-2*n; i-=2)
    {
        *write_to = (char) tolower(Alphabet::map_simple [ ((( uint64_t(0x03) <<i) & sequence) >> i) ] );
        write_to++;
    }
    return write_to;
}
char *write_sequence(char *write_to, uint64_t seq_id, uint64_t fwd_primer_start, uint64_t rev_primer_end)
{
    uint64_t start = fwd_primer_start > 0 ? fwd_primer_start :0;
    uint64_t end = (sequences[seq_id].sequence_length <rev_primer_end) ? sequences[seq_id].sequence_length : rev_primer_end;
    uint64_t k;

    k=start;
    for (; k+32<end; k+=32) {
        uint64_t seq_part= GET_SEQ_PRIMER(sequences[seq_id].sequence,k);

        write_to=write_sequence32(write_to,seq_part,31);
    }
    if (k<end) {
        uint64_t seq_part= GET_SEQ_PRIMER(sequences[seq_id].sequence,k);
        write_to=write_sequence32(write_to,seq_part,end-k-1);
    }
    return write_to;
}
//#define PRIMER3_WRITE_FILE
void collect_results(FILE *f=stdout)
{
    uint64_t picked=0;

    int pfdin[2],pfdout[2];

    pipe(pfdin);
    pipe(pfdout);

    pid_t pid=vfork();
    if (pid == 0) {
        //child
#ifdef PRIMER3_WRITE_FILE
        int outfd=open("primer3_output.txt", O_RDWR|O_CREAT, 0600);
        dup2(outfd,1);
#else
        dup2(pfdout[1],1);
#endif
        dup2(pfdin[0],0);

        execl(PRIMER3_PATH PRIMER3_NAME ,PRIMER3_NAME,"-p3_settings_file=./primer3-2.3.6/primer3_v1_1_4_default_settings.txt",(char *) 0);

    } else if (pid > 0) {
        //parent
        //exit(0);
        close(pfdin[0]);
        close(pfdout[1]);
#if !defined(PRIMER3_WRITE_FILE)
        FILE *fout=fdopen(pfdout[0],"r");

        fcntl(pfdout[0],F_SETFD,O_NONBLOCK);
#endif
        char *text=(char*)malloc(PRIMER3_TEXT_SZ);
        char *text_seq=(char*)malloc(TARGET_MAX_SZ+32+32);
        char *text_fwd=(char*)malloc(50);
        char *text_rev=(char*)malloc(50);

        for (uint64_t i=0; i<sequences.size(); i++) {
            std::deque< MatchMeta > seq_q;
            int picks_for_seq=0;


            for (uint64_t j=0; j<sequences[i].sequence_length; j++) {
                int tm;
                if (results[i].hits[j] >0 ) {
                    if (evaluate(GET_SEQ_PRIMER(sequences[i].sequence,j),results[i].hits[j],&tm)) {

                        while (!seq_q.empty() && (j - seq_q.front().position > TARGET_MAX_SZ)) {
                            seq_q.pop_front();
                        }
                        std::deque< MatchMeta >::iterator it=seq_q.begin();
                        for (; it!=seq_q.end(); it++) {
                            if ( abs(it->tm - tm) < PRIMER3_MAX_TEMPDIFF  && j - it->position > TARGET_MIN_SZ ) {
                                *write_sequence32(text_fwd,GET_SEQ_PRIMER(sequences[i].sequence,it->position),results[i].hits[it->position] )='\0';
                                *write_sequence32_rev_primer(text_rev,GET_SEQ_PRIMER(sequences[i].sequence,j),results[i].hits[j] )='\0';
                                *write_sequence(text_seq,i,it->position,j+results[i].hits[j] +1)='\0';
                                int tlength=snprintf(text,PRIMER3_TEXT_SZ,"PRIMER_TASK=pick_pcr_primers\n"
                                                     "SEQUENCE_ID=test\n"
                                                     "SEQUENCE_TEMPLATE=%s\n"
                                                     "SEQUENCE_PRIMER=%s\n"
                                                     "SEQUENCE_PRIMER_REVCOMP=%s\n"
                                                     "PRIMER_EXPLAIN_FLAG=1\n"
                                                     "=\n",
                                                     text_seq,
                                                     text_fwd,
                                                     text_rev);

                                write(pfdin[1],text,strlen(text));
#if !defined(PRIMER3_WRITE_FILE)
                                char *line=NULL;
                                size_t size;
                                size_t read=getline(&line,&size,fout);
                                bool left_ok=false,right_ok=false;
                                while (read>2)
                                {
                                    if (strcmp(line,"PRIMER_LEFT_EXPLAIN=considered 1, ok 1\n")==0) {
                                        left_ok=true;
                                    } else 	if (strcmp(line,"PRIMER_RIGHT_EXPLAIN=considered 1, ok 1\n")==0) {
                                        right_ok=true;
                                    }
                                    read=getline(&line,&size,fout);
                                }
                                if (left_ok && right_ok) {
                                    fprintf(f,"FWD=%s\n",text_fwd);
                                    fprintf(f,"REV=%s\n\n",text_rev);
                                    picks_for_seq++;
                                    picked++;
                                }
                                free(line);
#endif
                            }


                        }
                        seq_q.push_back(MatchMeta(j,tm));

                    }
                }
// 				if (picks_for_seq > PRIMER3_MAX_PICK_SZ ){
// 					std::cout<<"ss"<<std::endl;
// 					break;
//
// 				}
            }
        }
        free(text);
        free(text_fwd);
        free(text_rev);
        free(text_seq);

        close(pfdin[1]);
#if !defined(PRIMER3_WRITE_FILE)
        fclose(fout);
#endif

    } else {
        std::cout<<"fork() failed"<<std::endl;
    }
    std::cout<<"picked:"<<picked<<std::endl;
}
void print_results(FILE *f=stdout)
{
    for (size_t i=0; i<sequences.size(); i++) {
        for (uint64_t j=0; j<sequences[i].sequence_length; j++) {
            if (results[i].hits[j] >0 ) {
                char buff[33];
                //printf("%zu %u %u\n",i,j,results[i].hits[j]);
                *write_sequence32(buff,GET_SEQ_PRIMER(sequences[i].sequence,j),results[i].hits[j]-1)='\0';
                fprintf(f,"%s\n",buff);
            }
        }
    }
}
void print_results_pos(FILE *f=stdout)
{
    for (size_t i=0; i<sequences.size(); i++) {
        for (uint64_t j=0; j<sequences[i].sequence_length; j++) {
            if (results[i].hits[j] >0 ) {
                fprintf(f,"%zu %lu %u\n",i,j,results[i].hits[j]);

            }
        }
    }
}
void init_results()
{
    results.resize(sequences.size());
    for (int i=0; i<sequences.size(); i++) {
        int size=sequences[i].sequence_length;
        results[i].hits = static_cast<uint8_t*>(malloc(size));
        memset(results[i].hits,0,size);
    }
}
void deinit_results()
{
	for (auto &r : results)
	{
		free(r.hits);
	}
}
// fix the endings
void init_bloom(uint64_t total, hash_func hash, int primer_max_sz, uint8_t **bloom, uint8_t **bloom_dup, uint64_t *bloom_size)
{

	uint64_t bloom_range;
   
    *bloom_size = (total/8+1);
    *bloom=(uint8_t*) malloc(*bloom_size);
    *bloom_dup=(uint8_t*) malloc(*bloom_size);

	bloom_range = (*bloom_size)*8;
	
    // zero bloom
    memset(*bloom, 0, *bloom_size);
    memset(*bloom_dup, 0, *bloom_size);

    for (size_t j=0; j<sequences.size(); j++)
    {

        for (int i=0; i<sequences[j].regions.size(); i++) {
            uint64_t k=sequences[j].regions[i].start;
            uint64_t kmax=sequences[j].regions[i].end;
            //printf("%u %u %u\n",j,k,kmax);
            while (k+primer_max_sz<kmax+1) {

                uint64_t match=GET_SEQ_PRIMER(sequences[j].sequence,k);///first;

                uint64_t idx=hash(match>>(64-2*primer_max_sz))  % bloom_range;

                uint64_t bx= (*bloom)[idx/8] >> (idx & 7);
                uint64_t bdupx= (*bloom_dup)[idx/8] >> (idx & 7);


                //Alphabet::print_sequence_simple(match>>(64-2*primer_max_sz));


                //std::cout<<std::endl;
                //getchar();

                if (bx == 0x00 && bdupx == 0x00 ) {
                    (*bloom)[idx/8] |= 0x01U <<(idx & 7);
                } else if (bx == 0x01 && bdupx == 0x00 ) {
                    (*bloom_dup)[idx/8] |= 0x01U <<(idx & 7);
                }


                k++;
            }

        }
    }
    std::cerr<<"hash set "<<primer_max_sz<<std::endl;
}
void deinit_bloom(uint8_t **bloom, uint8_t **bloom_dup)
{
    free(*bloom);
    free(*bloom_dup);
}
void reset_sequences_check(std::vector<bool>& sequences_check)
{
    sequences_check.clear();
    sequences_check.resize(sequences.size());
    for (size_t i=0; i<sequences.size(); i++) {
        sequences_check[i]=true;
    }
}
void search(uint64_t total, int primer_max_sz, std::vector<bool>& no_matches)
{
    fprintf(stderr,"scanning for size %d\n",primer_max_sz);

    uint8_t *bloom,*bloom_dup;
    uint64_t bloom_size;
    hash_func hash = CRC;//hash_funcs[rand() % 2];
    uint64_t bloom_range;
	
	init_bloom(total, hash, primer_max_sz, &bloom, &bloom_dup, &bloom_size);
	bloom_range = (bloom_size *8);
	
    for (size_t j=0; j<sequences.size(); j++)
    {
        int ambi=0;

        tot_sl=0;
        for (int i=0; i<sequences[j].regions.size(); i++) {
            uint64_t k=sequences[j].regions[i].start;
            uint64_t kmax=sequences[j].regions[i].end;
            //printf("%u\n",kmax);
            while ( k+primer_max_sz<kmax+1) {

                uint64_t match=GET_SEQ_PRIMER(sequences[j].sequence,k);

                uint64_t idx=hash(match>>(64-2*primer_max_sz)) % bloom_range;
                //std::cout<<(std::bitset<64>(FNV(idx)))<<std::endl;
                uint64_t bx= bloom[idx/8] >> (idx & 7);
                uint64_t bdupx= bloom_dup[idx/8] >> (idx & 7);

                //Alphabet::print_sequence_simple(match>>(64-2*primer_max_sz));
                /*char buff[33];
                *write_sequence32(buff,match,primer_max_sz-1)='\0';
                printf("%s\n",buff);
                */
                //	if (strcmp("cgaaatt",buff)==0) {
                //	printf("xx\n");
                ///	}

                //std::cout<<std::endl;
                //Alphabet::print_sequence_simple(match>>(64-2*primer_max_sz));


                //std::cout<<std::endl;

                if (bx == 0x01 && bdupx == 0x00 ) {


                    if(results[j].hits[k]  == 0) {
                        results[j].hits[k] = primer_max_sz;
                        tot++;

                    }

                    tot_sl++;

                }

                k++;
            }
        }
    }
    deinit_bloom(&bloom, &bloom_dup);
    primer_max_sz++;
    bool alldone=true;
    for (size_t i=0; i<no_matches.size(); i++)
    {
        if (no_matches[i])
            alldone=false;
    }
    if (primer_max_sz <= PRIMER_MAX_SZ && !alldone) {
        search(total,primer_max_sz,no_matches);
    }

}
void display_options()
{
    std::cout<<"Usage :"<<std::endl;
    for (int j=0; j<sizeof(options)/sizeof(ArgumentOption); j++) {
        std::cout<<options[j].short_switch<<" : \n\t"<<options[j].desc << std::endl;;
    }
}
void parse_options(int argc, char **argv)
{
    int i=1;
    bool matched;
    if (argc == 1)
    {
        display_options();
        exit(0);
    }
    while(i<argc) {
        matched=false;
        for (int j=0; j<sizeof(options)/sizeof(ArgumentOption); j++) {
            if (strcmp(options[j].short_switch.c_str(),argv[i])==0)
            {
                i++;

                for (int k=0; k<options[j].data_count && i<argc; k++) {
                    options[j].data = std::string(argv[i]);
                    i++;
                }
                options[j].present=true;
                matched=true;
                break;
            }
        }
        if (!matched) {
            display_options();
            exit(0);
        }

    }

}
void init_bitmaps()
{
    for (auto &a : sequences)
    {
        Bitmap b;

        b.total= a.sequence_length;
        b.sequence = (uint8_t *)malloc(a.sequence_length/8 +1);
		memset(b.sequence,0,a.sequence_length/8 +1);
		bitmaps.push_back(b);

    }
}
void mark_bitmaps()
{
	for (size_t i=0; i<sequences.size(); i++) {
        for (uint64_t j=0; j<sequences[i].sequence_length; j++) {
            if (results[i].hits[j] >0 ) {
                for (uint64_t x=j;x<j+results[i].hits[j];x++)
				{
					bitmaps[i].sequence[x/8] |= 0x01U <<(x & 7);
				}
            }
        }
    }
}
void save_bitmaps()
{
	FILE *fs=fopen(options[OPT_BITMAP].data.c_str(),"w");
	for (auto &b: bitmaps)
	{
		fwrite(&b.total,sizeof(b.total),1,fs);
		fwrite(b.sequence,1,b.total/8+1,fs);
	}
	fclose(fs);
}
void deinit_bitmaps()
{
	for (auto &b: bitmaps)
	{
		free(b.sequence);
	}
}
int main(int argc, char **argv)
{
    parse_options(argc, argv);

   // mlockall(MCL_FUTURE);

    if (options[OPT_INPUT].present) {

        if (options[OPT_OUTPUT].present)
        {
            foutput = fopen(options[OPT_OUTPUT].data.c_str(),"w");
        } else {
            foutput = stdout;
        }
        FileReader reader(options[OPT_INPUT].data);
        reader.GetData2Bits(sequences,&total,&boundaries);

        init_results();

        auto t1=std::chrono::high_resolution_clock::now();
        std::vector<bool> no_matches;
        reset_sequences_check(no_matches);
        matches.resize(sequences.size());
        search(total,4,no_matches);
        auto t2=std::chrono::high_resolution_clock::now();
        std::chrono::duration<double> timespan=std::chrono::duration_cast<std::chrono::duration<double>>(t2-t1);

        fprintf(stderr,"Time: %lf\n",timespan.count());
        fprintf(stderr,"Size: %zu\n",sequences.size());
        int no_matches_count=0;
        for (size_t i=0; i<no_matches.size(); i++)
        {
            if (no_matches[i]) {
                no_matches_count++;
                //std::cout<<i<<std::endl;
            }
        }
        //std::cout<<"No matches: "<<no_matches_count<<" from "<<sequences.size()<<std::endl;
        //std::cout<<"Uniques: "<<tot<<std::endl;
        //collect_results();
        if (options[OPT_OUTTYPE].data.compare("positions")==0)
        {
            print_results_pos(foutput);
        } else if (options[OPT_OUTTYPE].data.compare("uniques")==0)
        {
            print_results(foutput);
        } else if (options[OPT_OUTTYPE].data.compare("primer3")==0) {
            collect_results(foutput);
        } else if (options[OPT_BITMAP].present)
		{
			init_bitmaps();
			mark_bitmaps();
			save_bitmaps();
			deinit_bitmaps();
		}
        fclose(foutput);
		
		deinit_results();
		
    }
   
    /*
    char buff[33];
    *write_sequence32(buff, GET_SEQ_PRIMER(sequences[0].sequence,0),32)='\0';
    printf("%s\n",buff);*/
    return 0;
}

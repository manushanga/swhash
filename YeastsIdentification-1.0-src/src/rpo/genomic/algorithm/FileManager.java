/*
 * Copyright 2007 Rafal Pokrzywa
 * 
 * This file is part of YeastsIdentification by Rafal Pokrzywa
 * 
 * YeastsIdentification is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * TandemRepeats is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TandemRepeats; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
package rpo.genomic.algorithm;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import rpo.genomic.data.UniqueSequence;
import rpo.genomic.data.YeastSequence;

/**
 * The <code>FileManager</code> class implements basic I/O operations.
 * 
 * @author Rafal Pokrzywa
 */
public class FileManager {

    static String SEQUENCES_FOLDER = "res/sequences";
    
    public static String loadSequence(String name) {
    	return loadSequence(SEQUENCES_FOLDER, name);
    }
    
    private static String loadSequence(String directory, String name) {
        StringBuffer buffer = new StringBuffer();
        try {
        	if (!name.endsWith(".txt"))
        		name += ".txt";
            File inputFile = new File(directory + "/" + name); 
            FileReader fileReader = new FileReader(inputFile);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                buffer.append(line);
            }
            bufferedReader.close();
            fileReader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return buffer.toString();
    }

	public static ArrayList loadFolderSequences(String folderName) {
	
		File file = new File(folderName);
		File[] files = file.listFiles();
		if (files == null)
			files = new File[0];
		int i = 0;
		
		ArrayList yeastSequences = new ArrayList(files.length);
		for (; i < files.length; i++) {
			if (files[i].isDirectory())
				continue;
			String sequence = FileManager.loadSequence(folderName, files[i].getName());
			String number = files[i].getName();
			if (number.endsWith(".txt"))
				number = number.substring(0, number.lastIndexOf(".txt"));

			YeastSequence yeastSequence = new YeastSequence(number, 
					validateDNA(sequence) + YeastSequence.SEQUENCE_SEPARATOR);
			yeastSequences.add(yeastSequence);
		}
		return yeastSequences;
	}

	private static String validateDNA(String sequence) {
		StringBuffer buffer = new StringBuffer(sequence.length());
		for (int i = 0; i < sequence.length(); i++) {
			char c = sequence.charAt(i);
			if (c != 'a' && c != 'c' && c != 'g' && c != 't') {// && c != 'n') { 
				buffer.append(YeastSequence.SEQUENCE_SEPARATOR);
			} else
				buffer.append(c);
		}
		return buffer.toString();
	}

	public static int saveFinalReport(String reportName, Collection uniqueSequences, int species, int limit) {
    	HashSet identified = new HashSet(species);
        try {
            File outputFile = new File(reportName);
            FileWriter fileWriter = new FileWriter(outputFile);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write("Unique yeast species identified with YeastsIdentification by Rafal Pokrzywa of length up to " + limit + "\n");
            for (Iterator iter = uniqueSequences.iterator(); iter.hasNext();) {
                UniqueSequence uniqueSequence = (UniqueSequence) iter.next();
                if (limit == 0 || uniqueSequence.getLength() <= limit) {
                	bufferedWriter.write(uniqueSequence.getYeastSequence().getNumber() + ";" + uniqueSequence.getStart() + ";" 
                			+ uniqueSequence.getLength() + ";" + uniqueSequence.getSequence() + "\n");
                	identified.add(uniqueSequence.getYeastSequence().getNumber());
                }
            }
            bufferedWriter.write(identified.size() + " of " + species + " yeast species where identified with sequences of length up to " + limit);
            bufferedWriter.close();
            fileWriter.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return identified.size();
	}
	
}

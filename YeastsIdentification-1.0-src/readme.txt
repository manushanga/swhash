*** README file for YeastsIdentification ***

1. Introduction

The aim of this document is to illustrate how to install and use the application.


2. Software requirements

Java Runtime Environment (JRE) version 1.4


3. Application usage
  
java -cp bin rpo.genomic.algorithm.UniqueYeastSpecies [input] [limit] [output]

	input - folder containing input sequences
	limit - unique substring max length
	output - output report file name
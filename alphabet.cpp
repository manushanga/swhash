#include "alphabet.h"
#include <cstdlib>
#include <cstdio>
#include <ctime>

char Alphabet::map[]={'$','X','A','C','G','T'};
char Alphabet::map_simple[]={'A','C','G','T'};
char Alphabet::map_simple_rev[]={'T','G','C','A'};

uint64_t* Alphabet::generate_output(uint64_t n)
{
	return (uint64_t *) malloc(sizeof(uint64_t)*n);
}
void Alphabet::destroy_output ( uint64_t* output )
{
	free(output);
}

uint8_t* Alphabet::generate_input(uint64_t n)
{
	uint8_t *input=(uint8_t*) malloc(n);
	int alpha=size-1;
	srand(std::time(NULL));
	for (uint64_t i=0;i<n;i++){
		input[i]= (rand()&0x000000FF) % alpha + 1;
	}
	input[n-1]=0;
	return input;
}
void Alphabet::print_input_simple(uint8_t *input, uint64_t n)
{	
	for (uint64_t i=0;i<n;i++)
	{
		if (input[i] == 0)
			std::printf("$, ");
		else
			std::printf("%c, ",Alphabet::map_simple[input[i]-1]);
	}
	std::printf("\n");
	//std::cout<<std::endl;
}
void Alphabet::print_input(uint8_t *input, uint64_t n)
{	
	for (uint64_t i=0;i<n;i++)
	{
		//std::cout<<Alphabet::map[input[i]]<<", ";
		std::printf("%c, ",Alphabet::map[input[i]]);
	}
	std::printf("\n");
	//std::cout<<std::endl;
}
void Alphabet::print_input_array(uint8_t* input, uint64_t n)
{
    for (uint64_t i=0;i<n;i++)
    {
        //std::cout<<Alphabet::map[input[i]]<<", ";
        std::printf("%d, ",input[i]);
    }
    std::printf("\n");
}

void Alphabet::print_output(uint8_t *input, uint64_t *output, uint64_t n, uint64_t until)
{
	for (uint64_t i=0;i<until;i++)
	{	
		std::cout<<std::setw(3)<<i<<": ";
		for (uint64_t j=output[i];j<output[i]+until && j<n;j++)
		{
			std::cout<<(char) tolower(Alphabet::map[input[j]]);
		}
		std::cout<<std::endl;
	}
}
void Alphabet::print_output_simple(uint8_t *input, uint64_t *output, uint64_t n)
{
	for (int i=0;i<n;i++)
	{	
		std::cout<<std::setw(3)<<i<<": ";
		for (int j=output[i];j<n;j++)
		{
			if (input[j] == 0)
				std::cout<<'$';
			else
				std::cout<<(char)( Alphabet::map_simple[input[j]-1] );
		}
		std::cout<<std::endl;
	}
}

void Alphabet::print_sequence_simple(uint64_t sequence)
{
	for (int i=62;i>=0;i-=2)
	{
		std::cout<< (char) tolower(map_simple [ ((( uint64_t(0x03) <<i) & sequence) >> i) ] ); 
	}
}

void Alphabet::print_output(uint8_t *input, uint64_t *output, uint64_t n)
{
	for (int i=0;i<n;i++)
	{	
		std::cout<<std::setw(3)<<i<<": ";
		for (int j=output[i];j<n;j++)
		{
			std::cout<<(char) tolower( Alphabet::map[input[j]] );
		}
		std::cout<<std::endl;
	}
}
void Alphabet::print_output(uint64_t* output, uint64_t n)
{
	for (int i=0;i<n;i++)
	{
		std::cout<<output[i]<<", ";
	}
	std::cout<<std::endl;
}
void Alphabet::print_sequence(FILE *to, uint8_t* input, SASUniqueSequence* sequence)
{
	for (uint64_t i=sequence->start;i<=sequence->end;i++){
		std::fprintf(to,"%c", (char)tolower(Alphabet::map[input[i]]) );
	}
	std::fprintf(to,"\n");
}
void Alphabet::print_sequence(uint8_t* input, SASUniqueSequence* sequence)
{
	for (uint64_t i=sequence->start;i<=sequence->end;i++){
		std::cout<<(char)tolower(Alphabet::map[input[i]]);
	}
	std::cout<<std::endl;
}
void Alphabet::print_sequence_2bit(uint64_t seq, int len)
{
	for (int i=0;i<len;i++){
		std::cout<<Alphabet::map_simple[(2*i<<seq)>>(62-2*i)];
	}
	std::cout<<std::endl;
}



#include <iostream>
#include <chrono>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <vector>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <string>

#include "filereader.h"
#include "naive.h"
#include "naive_bsort.h"

#include "graph.h"

#ifdef QT
#include <QApplication>
#include "mainwindow.h"
#endif

typedef SASFunc* (*creator)();
creator funcs[]={SASNaive::create, SASNaiveBSort::create};

enum data_mode {DM_CHAR=0,DM_2BIT};
const char *names[] = {"Naive","Naive with Bucket Sort"};
const data_mode data_modes[] = { DM_CHAR, DM_CHAR };
const char *colors[] = {"Blue","Red"};
std::vector< std::vector<PlotPoint> > points;
double maxtime=0,maxmemory=0;

struct ArgumentOption{
	std::string long_switch;
	std::string short_switch;
	std::string description;
	int data_count;
	std::vector<std::string> data;
	bool present;
};

#define OPT_OUTPUT 0
#define OPT_INPUT 1
#define OPT_INPUT_SEQ 2
#define OPT_INPUT_UNQ_LEN 3
#define OPT_INPUT_UNQ_CNT 4
#define OPT_GRAPH 5
#define OPT_DRY 6
#define OPT_TEST 7
#define OPT_OUTTYPE 8
#define OPT_BITMAP 9
#define OPT_HELP 10

ArgumentOption options[] = 
{ 
	{"--output", "-o","Output file",1,std::vector<std::string>(),false},
	{"--input", "-i","input file",1,std::vector<std::string>(),false},
	{"--input-sequence", "-is","Comma separated input files",1,std::vector<std::string>(),false},
	{"--max-unique-length", "-maxlen","Max length for a single unique sequence",1,std::vector<std::string>(),false},
	{"--max-unique-count", "-maxcount","Limit the number of unique sequences found",1,std::vector<std::string>(),false},
	{"--graph", "-g","Draw Graphs",0,std::vector<std::string>(),false},
	{"--dry", "-d","Dry run, doesn't output",0,std::vector<std::string>(),false},
	{"--test", "-t","Comma separated list of tests",1,std::vector<std::string>(),false},
	{"--outtype","-ot","Output type uniques or positions",1,std::vector<std::string>(),false},
	{"--map","-m","output a binary map file, takes an argument as filename",1,std::vector<std::string>(),false},
	{"--help", "-h","Display help",0,std::vector<std::string>(),false}

	
};

struct Bitmap {
    uint64_t total;
    uint8_t *sequence;
};
std::vector<Bitmap> bitmaps;

void split(std::string str, char delim, std::vector<std::string>& strvec)
{
    char *sptr=(char*)str.c_str(),*eptr=(char*)str.c_str();
    while (*eptr!='\0'){
        if (*eptr == delim){
            char tmp = *eptr;
            *eptr='\0';
            if (*sptr != '\0')
                strvec.push_back( std::string(sptr) );
            *eptr=tmp;
            sptr=eptr+1;
        }
        eptr++;
    }
    if (*sptr != '\0')
        strvec.push_back( std::string(sptr) );
}

void display_options()
{
	std::cout<<"Usage :"<<std::endl;
	for (int j=0;j<sizeof(options)/sizeof(ArgumentOption);j++){
		std::cout<<options[j].short_switch<<", "<<options[j].long_switch<<" : \n\t"<<options[j].description << std::endl;;
	}
}
void parse_options(int argc, char **argv)
{
	int i=1;
	bool matched;
	while(i<argc){
		matched=false;
		for (int j=0;j<sizeof(options)/sizeof(ArgumentOption);j++){
			if ((strcmp(options[j].long_switch.c_str(),argv[i])==0) ||
				(strcmp(options[j].short_switch.c_str(),argv[i])==0))
			{
				i++;
				
				for (int k=0;k<options[j].data_count && i<argc;k++){
					options[j].data.push_back(std::string(argv[i]));
					i++;
				}
				options[j].present=true;
				matched=true;
				break;
			}
		}
		if (!matched){
			display_options();
			exit(0);
		}
		
	}
	
}
void process_mem_usage(double& mem, double& all_mem)
{
   using std::ios_base;
   using std::ifstream;
   using std::string;

   mem = 0.0;
   ifstream stat_stream("/proc/self/statm",ios_base::in);

   unsigned long size,rss,share,text,lib,data,dt;

   stat_stream >> size >> rss >> share >> text >> lib >> data >> dt;

   stat_stream.close();

   long page_size_kb = sysconf(_SC_PAGE_SIZE) / 1024; // in case x86-64 is configured to use 2MB pages
   mem = (rss - share) * page_size_kb;
   all_mem = rss * page_size_kb;
}

void check_options()
{
	if ((!options[OPT_INPUT].present || !options[OPT_INPUT_SEQ].present ) &&
		(!options[OPT_INPUT_UNQ_LEN].present || options[OPT_HELP].present) )
	{
		std::cout<<"Error: Max Length for a unique sequence(-maxlen) and input(-i or -is) is "
				 <<"compulsory!"<<std::endl;
		display_options();
		exit(0);
	}
	
	for (int j=0;j<sizeof(options)/sizeof(ArgumentOption);j++){
		if (options[j].present){
			if ( options[j].data.size() != options[j].data_count)  {
				std::cout<<"Error: Doesn't match parse rules"<<std::endl;
				display_options();
				exit(0);
				break;
			}
		}
	}
}


void init_bitmaps(uint64_t *boundaries, uint64_t boundary_count)
{
	uint64_t i=0;
	
	uint64_t prev_b=0;

    while (i<boundary_count)
    {
        Bitmap b;

        b.total= boundaries[i]-prev_b-1;
		prev_b=boundaries[i];
	
        b.sequence = (uint8_t *)malloc(b.total/8 +1);
		memset(b.sequence,0,b.total/8 +1);
		bitmaps.push_back(b);
		i++;
    }
}
inline void mark_bitmaps(uint64_t i, uint64_t x)
{

	bitmaps[i].sequence[x/8] |= 0x01U <<(x & 7);
}
void save_bitmaps()
{
	FILE *fs=fopen(options[OPT_BITMAP].data[0].c_str(),"w");
	for (auto &b: bitmaps)
	{
		fwrite(&b.total,sizeof(b.total),1,fs);
		fwrite(b.sequence,1,b.total/8+1,fs);
	}
	fclose(fs);
}

int main(int argc, char **argv) {
	uint8_t *input,*input_bits;
	uint64_t total,*boundaries,boundary_count;

	parse_options(argc, argv);
	check_options();
	SET_DEBUG_LEVEL(1);
	

	/*uint8_t* aa = Alphabet::generate_input(1000000);
	std::FILE *f=std::fopen("uniform","w");
	std::fwrite(aa,1,1000000,f);
	std::fclose(f);
	exit(0)*/
	std::vector<std::string> files;
	std::vector<int> tests;
	std::string output_file;
	int data_modes_count[sizeof(data_mode)]={0};
	
	if (options[OPT_INPUT_SEQ].present){
		split(options[OPT_INPUT_SEQ].data[0],',',files);
	} else {
		files.push_back(options[OPT_INPUT].data[0]);
	}
	
	if (options[OPT_TEST].present){
		std::vector<std::string> tests_;
		split(options[OPT_TEST].data[0],',',tests_);
		for (int i=0;i<tests_.size();i++) {
			int test =atoi(tests_[i].c_str());
			tests.push_back(test);
			data_modes_count[ data_modes[test] ]++;
		}
	} else {
		for (int idx_func=0;idx_func<sizeof(funcs)/sizeof(creator);idx_func++){
			tests.push_back(idx_func);
			data_modes_count[ data_modes[idx_func] ]++;
		}
	}
	if (options[OPT_OUTPUT].present){
		output_file = options[OPT_OUTPUT].data[0];
	}
	points.resize(sizeof(colors)/sizeof(char *));
	for (int idx_file=0;idx_file<files.size();idx_file++)
	{
		FileReader reader(files[idx_file]);
		
		if (data_modes_count[DM_CHAR])
			reader.GetData(&input,&boundaries,&total, &boundary_count);
		//reader.DumpData("xx.txt");
		std::vector<bool> no_matches;
		no_matches.resize(boundary_count);
		
		std::cerr<<"\nFile: "<<files[idx_file]<<std::endl;
		uint64_t *output=Alphabet::generate_output(total);
		
		FILE *fo;
		if (output_file.size()>0) {
			fo=fopen(output_file.c_str(),"w");
		} else {
			fo=stdout;
		}
		for (int idx_test=0;idx_test<tests.size();idx_test++){
			std::cerr<<"\nTesting Algorithm: "<<names[tests[idx_test]]<<std::endl;
			for(size_t i=0;i<boundary_count;i++)
			{
				no_matches[i]=true;
			}
			//init_bitmaps(boundaries,boundary_count);

			printf("%lu %lu\n",total,boundary_count);
		
			SASFunc *obj= funcs[tests[idx_test]]();
			obj->SASSetLength(total);
			obj->SASSetUniqueMaxLength(atoi (options[OPT_INPUT_UNQ_LEN].data[0].c_str() ));
			obj->SASSetSequencesTotal(boundary_count);
			std::cerr<<boundary_count<<std::endl;
			auto t1=std::chrono::high_resolution_clock::now();
			obj->SASRun(input,output);
			auto t2=std::chrono::high_resolution_clock::now();
			
			std::chrono::duration<double> timespan=std::chrono::duration_cast<std::chrono::duration<double>>(t2-t1);
			
			SASUniqueSequence seq;
			SASUniqueSequenceIterator iter;
			uint64_t i=0,j=0,max_unqs=obj->SASGetLength()-1;
			
			//Alphabet::print_output(input, output, total);
			if (options[OPT_DRY].present) {
				if (options[OPT_INPUT_UNQ_CNT].present)
				{
					max_unqs=(uint64_t) atoi(options[OPT_INPUT_UNQ_CNT].data[0].c_str());
				}		
				while (j<max_unqs && i<obj->SASGetLength()-1){
					obj->SASNextUniqueSequence(input,output,&iter,&seq);
					if (seq.start!=(uint64_t)-1UL) {
						j++;
						//Alphabet::print_sequence(fo,input, &seq);
						//std::fprintf(fo,"%d\n",reader.GetSequence(seq.start));
						//uint64_t ss=reader.GetSequence(seq.start);
						//fprintf(fo,"%u %u %u\n",ss,boundaries[ss]- seq.start, seq.end -seq.start);
					}
					i++;
				}
			} else {
				auto t1x=std::chrono::high_resolution_clock::now();
				if (options[OPT_OUTTYPE].present) 
				{
					if (options[OPT_OUTTYPE].data[0].compare("positions")==0)
					{
						while (j<max_unqs && i<obj->SASGetLength()-1){
							obj->SASNextUniqueSequence(input,output,&iter,&seq);
							//std::cout<<"d"<<std::endl;
							if (seq.start!=(uint64_t)-1UL) {
								j++;
								no_matches[reader.GetSequence(seq.start)]=false;
			
								uint64_t ss=reader.GetSequence(seq.start);
								uint64_t seq_start= (ss==0)?0:boundaries[ss-1]+1;
								fprintf(fo,"%lu %lu %lu\n",ss,seq.start -seq_start, seq.end -seq.start+1);
								//std::fprintf(fo,"%d\n",reader.GetSequence(seq.start));
							}
							i++;
						}
					} else if (options[OPT_OUTTYPE].data[0].compare("uniques")==0)
					{
						while (j<max_unqs && i<obj->SASGetLength()-1){
							obj->SASNextUniqueSequence(input,output,&iter,&seq);
							//std::cout<<"d"<<std::endl;
							if (seq.start!=(uint64_t)-1UL) {
								j++;
								no_matches[reader.GetSequence(seq.start)]=false;

								Alphabet::print_sequence(fo,input,&seq);
							}
							i++;
						}
					} 
				}
				
				if (options[OPT_BITMAP].present) {
					init_bitmaps(boundaries,boundary_count);
					while (j<max_unqs && i<obj->SASGetLength()-1){
						obj->SASNextUniqueSequence(input,output,&iter,&seq);
						//std::cout<<"d"<<std::endl;
						if (seq.start!=(uint64_t)-1UL) {
							j++;
							no_matches[reader.GetSequence(seq.start)]=false;

							uint64_t ss=reader.GetSequence(seq.start);
							uint64_t seq_start= (ss==0)?0:boundaries[ss-1]+1;
							//fprintf(fo,"%u %u %u\n",ss,seq.start -seq_start, seq.end -seq.start+1);
							
							for (uint64_t i=seq.start -seq_start;i<seq.end -seq_start +1;i++)
							{
								mark_bitmaps(ss,i);
							}
						}
						i++;
					}
					save_bitmaps();
				}
					
				auto t2x=std::chrono::high_resolution_clock::now();
				std::chrono::duration<double> timespanx=std::chrono::duration_cast<std::chrono::duration<double>>(t2x-t1x);
				std::cout<<"Time for SA: "<<timespan.count()<<std::endl;
				std::cout<<"Time for Unique List: "<<timespanx.count()<<std::endl;
				double average_size=double(total)/double(boundary_count);
				std::cout<<"Average Sequence Length: "<<average_size<<std::endl;
				double private_m, total_m;
				process_mem_usage(private_m, total_m);
				std::cout<<"private and total memory: "<<private_m<<" "<< total_m<<std::endl;
				std::cout<<"Uniques: "<<j<<std::endl;
				points[idx_test].push_back(PlotPoint(double(boundary_count),average_size,timespan.count(),private_m));

			}
			if (fo!=stdout)
				fclose(fo);
			uint64_t no_matches_count=0;
			for (size_t i=0;i<no_matches.size();i++)
			{
				if (no_matches[i])
					no_matches_count++;
			}
			std::cerr<<"No matches:"<<no_matches_count<<std::endl;
		}
		Alphabet::destroy_output(output);
		
	}
	
    Graph g;

    for (int i=0;i<tests.size();i++)
    {
        g.algos.push_back(names[tests[i]]);
        g.colors.push_back(colors[tests[i]]);
    }
    for (int i=0;i<files.size();i++)
        g.inputs.push_back(files[i]);
    g.points = points;
#ifdef QT
#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
    QApplication::setGraphicsSystem("raster");
#endif
    QApplication a(argc, argv);
    MainWindow wm(&g,DrawType::DrawMemory);
    MainWindow wt(&g,DrawType::DrawTime);
    MainWindow wmi(&g,DrawType::DrawMemoryPerInput);


    wm.show();
    wt.show();
    wmi.show();

    return a.exec();
#else
	
	return 0;
#endif

}


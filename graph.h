#ifndef GRAPH_H
#define GRAPH_H
#include <string>
#include <vector>

struct PlotPoint{
    double input_size;
    double avg_size;
    double time;
    double memory;
    PlotPoint(double i, double a,double t,double m) :
        input_size(i),
        avg_size(a),
        time(t),
        memory(m)
    {}
};

struct Graph
{

    std::vector< std::vector<PlotPoint> > points;
    std::vector< std::string > inputs;
    std::vector<std::string> algos;
    std::vector<std::string> colors;

};

enum DrawType{DrawTime, DrawMemory, DrawMemoryPerInput};
#endif // GRAPH_H

#ifndef __TEST_FUNC
#define __TEST_FUNC
#include "debug.h"
#include "alphabet.h"
#include "sequence.h"
/*
 * The interface for benchmark functions
 */
class SASFunc
{
protected:
	uint64_t sequences_total;
	uint64_t total;
	uint64_t unique_max_length;
public:
	virtual uint64_t SASGetLength() { return total; }
	virtual void SASSetLength(uint64_t n){ total = n; }
	virtual void SASSetSequencesTotal(uint64_t sequences) { sequences_total = sequences; }
	virtual void SASSetUniqueMaxLength(uint64_t length){ unique_max_length=length; }
	virtual void SASRun(void *input, void *output) = 0;
	virtual void SASNextUniqueSequence(uint8_t* input, uint64_t* output, SASUniqueSequenceIterator *iterator, SASUniqueSequence *sequence) = 0;
	virtual ~SASFunc() {};
};
#endif
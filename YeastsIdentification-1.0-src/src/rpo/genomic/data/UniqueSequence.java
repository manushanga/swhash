/*
 * Copyright 2007 Rafal Pokrzywa
 * 
 * This file is part of YeastsIdentification by Rafal Pokrzywa
 * 
 * YeastsIdentification is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * TandemRepeats is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TandemRepeats; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
package rpo.genomic.data;

/**
 * The <code>UniqueSequence</code> class represents an unique yeast sequence. 
 * 
 * @author Rafal Pokrzywa
 */
public class UniqueSequence {
	
	private YeastSequence yeastSequence;
	
	private int start;

	private int length;

	public UniqueSequence(YeastSequence yeastSequence, int start, int length) {
		this.yeastSequence = yeastSequence;
		this.start = start;
		this.length = length;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public YeastSequence getYeastSequence() {
		return yeastSequence;
	}

	public void setYeastSequence(YeastSequence yeastSequence) {
		this.yeastSequence = yeastSequence;
	}

	public String getSequence() {
		if (start + length - 1 > yeastSequence.getSequence().length())
			System.out.println(yeastSequence.getSequence().length() + " " + (start + length - 1));
		return yeastSequence != null ? yeastSequence.getSequence().substring(start - 1, start + length - 1) : null;
	}
	
}

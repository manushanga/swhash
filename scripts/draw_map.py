import sys
from PIL import Image
h=int(sys.argv[4])
w=int(sys.argv[5])
def compare(fa,fb,fsave):
	img1 = Image.new( 'RGB', (w,h), "white") # create a new black image
	img2 = Image.new( 'RGB', (w,h), "white") # create a new black image
	img3 = Image.new( 'RGB', (w,h), "white") # create a new black image

	pixels1 = img1.load() # create the pixel map
	pixels2 = img2.load() # create the pixel map
	pixels3 = img3.load() # create the pixel map

	fdatafull=open(fa,"r")
	
	lines = fdatafull.readlines(10000000)
	while (len(lines) > 0):
		for line in lines:

			line=[int(a) for a in line.split(' ')]
			
			for i in range(line[1],line[1]+line[2]):
				pixels1[i,line[0]*2 ]=(0,0,0)
		lines = fdatafull.readlines(10000000) 
	fdatafull.close()
	
	fdata=open(fb,"r")
	
	lines = fdata.readlines(10000000)
	while (len(lines) > 0):
		for line in lines:
			
			line=[int(a) for a in line.split(' ')]
			for i in range(line[1],line[1]+line[2]):
				pixels2[i,line[0]*2]=(0,0,255)
		lines = fdata.readlines(10000000) 
	fdata.close()
	reds=0
	blacks=0
	for i in range(w):
		for j in range(h):
			if (pixels1[i,j]==(0,0,0) and pixels2[i,j]==(0,0,255)):
				pixels3[i,j]=(0,0,0)
				blacks+=1
			elif pixels1[i,j]==(0,0,0):
				pixels3[i,j]=(255,0,0)
				reds+=1
			elif pixels2[i,j]==(0,0,255):
				pixels3[i,j]=(0,0,255)
	print(fsave,"b",blacks,"r",reds,(reds*100.0)/(blacks+reds))
	img3.save(fsave+'.png')
if __name__ == "__main__":
	compare(sys.argv[1],sys.argv[2],sys.argv[3])
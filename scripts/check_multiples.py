import os
# Madura A.
# Checks for multiple occurences of substrings in ../out.txt 
# ../sequences.txt is used as the text to check against
#
import subprocess
import sys

MAX_BATCH_SIZE=20
ss=set()
batch=0
sequences=""
if (len(sys.argv) <3):
	print("Usage: check_multiples.py uniques_list.txt sequences.txt")
else:
	ffr = open(sys.argv[1],'r')
	ffx = open(sys.argv[2],'r')
	line = ffx.readline()
	readname=False
	while line!="":
		sline = line.strip()
		if (not sline.startswith('>')):
			sequences += sline.lower()
			readname=False
		else:
			if (readname):
				exit(0)
			sequences += '\n'
			readname=True
		line=ffx.readline()
		
	regex=""
	

	while True:
		line = ffr.readline().lower()
		
		if line=='':
			break
		else:
			line=line.strip()
			cc=sequences.count(line)
			if (cc != 1):
				print(line) 
				print("fail",cc)
	ffr.close()
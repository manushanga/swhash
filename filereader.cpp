#include "filereader.h"
#include "alphabet.h"
#include "debug.h"
/*
 Files should follow FASTA format and should not include any other characters
 than allowed.
 */
using namespace std;
FileReader::FileReader(std::string filename)
{
	uint64_t total_read=0;
	uint64_t sequence_length=0;
	std::vector< SASRegion > regions;
	bool first_seen_alpha=false;
	bool last_seen_is_ambi=false;
	mfilename = filename;
	mtotal=0;
	FILE *file=fopen(mfilename.c_str(),"r");
	int state=0;
	mboundary_count=0;
	
	int c=fgetc(file);
	while (!feof(file)){
		switch (state){
			case 0:
			{
				regions = std::vector<SASRegion>();

				if (c=='>')
					state=1;
				break;
			}
			case 1:
			{
				if (c=='\n')
					state=2;
				break;
			}
			case 2:
			{
				int cc=toupper(c);
				if (c == '>'){
					if (!last_seen_is_ambi){
						regions.back().end = sequence_length;
					}
				
					SASSequence seq;
					seq.sequence_length = sequence_length;
					seq.regions = regions;
					

					sequences_.push_back(seq);
					mboundary_count++;
					state=1;
					sequence_length=0;
					regions = std::vector<SASRegion>();
					first_seen_alpha=false;
					mtotal++;
					last_seen_is_ambi=false;
				} else if (cc=='A' || cc=='C' || cc=='G' || cc=='T'){
					
					if (last_seen_is_ambi || !first_seen_alpha){
						SASRegion ambi;
						regions.push_back(ambi);
						regions.back().start = sequence_length;
						first_seen_alpha=true;


					}
					sequence_length++;
					last_seen_is_ambi=false;
					mtotal++;
				} else if (cc=='U' || cc=='R' || cc=='Y' || cc=='K' || 
						cc=='M' || cc=='S' || cc=='W' || cc=='B' || 
						cc=='D' || cc=='H' || cc=='V' || cc=='N' ||
						cc=='X' || cc=='-')
				{
					
					if (!last_seen_is_ambi && regions.size()>0 ) {
						regions.back().end = sequence_length;
						
					} 
					
					sequence_length++;
					mtotal++;
					last_seen_is_ambi=true;
				} else if (c=='\n') {
				} else {
					ERR("Unknown character");
				}
				break;
			}
		}
		c=fgetc(file);
	}
	if (!last_seen_is_ambi && regions.size()) {
		
		regions.back().end = sequence_length;
	}
	
	SASSequence seq;
	seq.sequence_length = sequence_length;
	seq.regions = regions;
	sequences_.push_back(seq);
	mdata=NULL;
	mboundaries=NULL;
	mtotal++;
	mboundary_count++;
	fclose(file);
	std::cerr<<"read done"<<std::endl;

}
void FileReader::GetData(uint8_t** data, uint64_t** boundaries, uint64_t* total, uint64_t *total_boundaries)
{
	
	FILE *file=fopen(mfilename.c_str(),"r");
	int state=0;
	uint64_t write=0,bwrite=0;
	uint64_t sequence_length=0;
	uint8_t rev[256];
	for (int i=0;i<Alphabet::size;i++)
		rev[Alphabet::map[i]]=i;
	// FASTA format read

	mdata = (uint8_t *)malloc(mtotal+1);
	mboundaries = (uint64_t*)malloc(sizeof(uint64_t)*mboundary_count);

	int c=fgetc(file);
	while (!feof(file)){
		switch (state){
			case 0:
			{
				if (c=='>')
					state=1;
				break;
			}
			case 1:
			{
				if (c=='\n')
					state=2;
				break;
			}
			case 2:
			{
				int cc=toupper(c);
				if (c == '>'){
					state=1;
					mdata[write]=ALPHA_SEQ_END;//rev[Alphabet::map[0]];
					mboundaries[bwrite]=write;
					bwrite++;
					write++;
				} else if (cc=='A' || cc=='C' || cc=='G' || cc=='T'){
					mdata[write]=rev[cc];
					write++;
				} else if (cc=='U' || cc=='R' || cc=='Y' || cc=='K' || 
						cc=='M' || cc=='S' || cc=='W' || cc=='B' || 
						cc=='D' || cc=='H' || cc=='V' || cc=='N' ||
						cc=='X' || cc=='-')
				{
					mdata[write]=ALPHA_AMBIGUOUS;
					write++;
				} else if (c=='\n')
				{
					
				} else {
					ERR("unknown character");
				}
				break;
			}
		}
		c=fgetc(file);
	}
	
	mdata[write] = ALPHA_SEQ_END;
	mboundaries[bwrite] = write;
	*data=mdata;
	*boundaries=mboundaries;
	*total=mtotal;
	*total_boundaries=mboundary_count;
	fclose(file);
}
void FileReader::GetData2Bits(std::vector< SASSequence >& sequences, uint64_t* total, uint64_t* boundary_count)
{

	FILE *file=fopen(mfilename.c_str(),"r");
	int state=0;
	int write=0,bwrite=0;
	uint64_t sequence_length=0;
	uint64_t current_sequence=0;
	uint64_t rev[256];
	rev['A']=0;
	rev['C']=1;
	rev['G']=2;
	rev['T']=3;
	
	// FASTA format read
	for (size_t i=0;i<sequences_.size();i++)
	{
		sequences_[i].sequence = (uint64_t*) malloc(sequences_[i].sequence_length/4 + 16);
		memset(sequences_[i].sequence,0,sequences_[i].sequence_length/4 + 16);
	}
	
	// recount total because previous value includes
	// sentinels
	
	mtotal = 0; 
	int c=fgetc(file);
	while (!feof(file)){
		switch (state){
			case 0:
			{
				if (c=='>')
					state=1;
				break;
			}
			case 1:
			{
				if (c=='\n')
					state=2;
				break;
			}
			case 2:
			{
				int cc=toupper(c);
				if (c == '>'){
					state=1;
					
					mtotal+=sequence_length;
					sequence_length=0;
					current_sequence++;
				} else if (cc=='A' || cc=='C' || cc=='G' || cc=='T'){
					uint64_t *sequence= reinterpret_cast<uint64_t*>(sequences_[current_sequence].sequence);
					//DBG(0,(uint64_t)sequence<<" "<<sequence_length);
					sequence[sequence_length/32] |=rev[cc]<<(62-2*(sequence_length%32));
					//DBG(0,current_sequence);
					sequence_length++;
				} else if (cc=='U' || cc=='R' || cc=='Y' || cc=='K' || 
						cc=='M' || cc=='S' || cc=='W' || cc=='B' || 
						cc=='D' || cc=='H' || cc=='V' || cc=='N' ||
						cc=='X' || cc=='-')
				{
					// not needed to write here we are defaulting to 0b00
					sequence_length++;
				} 
				break;
			}
		}
		c=fgetc(file);
	}
	
	sequences=sequences_;
	*total=mtotal;
	//*total_boundaries=mboundary_count;
	fclose(file);
}

void FileReader::DumpData(string filename)
{
	FILE *fo=fopen(filename.c_str(),"w");
	if (mdata) {
		for (uint64_t i=0;i<mtotal;i++)
		{
			fprintf(fo,"%c",Alphabet::map[mdata[i]]);
		}
	} else {
		for (size_t i=0;i<sequences_.size();i++)
		{
			for (uint64_t j=0;j<sequences_[i].sequence_length/4+1;j++){
				uint8_t t= reinterpret_cast<uint8_t *>( sequences_[i].sequence)[j];
				fprintf(fo,"%c%c%c%c", Alphabet::map_simple[ t>>6 ], 
						Alphabet::map_simple[t>>4 & 0x03] ,
						Alphabet::map_simple[t>>2 & 0x03] ,
						Alphabet::map_simple[t & 0x03] );
			
			}
			fprintf(fo,"\n");
			
		}
		
	}
	fclose(fo);
}

uint64_t FileReader::GetSequence(uint64_t offset)
{
	uint64_t lo=0,hi=mboundary_count-1,mid=0, mid_seq;
	
	// we get out of the loop as soon as difference between 
	// hi and lo are 1 if offset is not in the list this is 
	// not going to be any better
	
	while (hi-lo>1){
		mid=(lo+hi)/2;
		mid_seq=mboundaries[mid];
		if (mid_seq == offset){
			hi=lo=mid;
		} else if (mid_seq > offset){
			hi=mid;
		} else {
			lo=mid;
		}
	}
	
	// the case where the above loop breaks is when 
	// hi == lo +1 therefore, the two elements are
	// adjacent therefore if offset is lower than
	// the lower item answer should be lo otherwise
	// it must be lower than mboundaries[hi]
	if (mboundaries[lo] > offset)
		return lo;
	else // if (mboundaries[hi] > offset)
		return hi;
}

FileReader::~FileReader()
{
	free(mboundaries);
	free(mdata);
}

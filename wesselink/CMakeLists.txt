cmake_minimum_required(VERSION 2.6)
project(wesselink)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_CFLAGS} -std=c++11")

add_executable(wesselink main.cpp)

install(TARGETS wesselink RUNTIME DESTINATION bin)

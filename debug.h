extern int __debug_level;
#ifndef __H_DEBUG
#define __H_DEBUG
#define SET_DEBUG_LEVEL(x) __debug_level=x
#define DBG(level, ... ) \
	if (level<__debug_level)\
	{\
		std::cerr<<__FUNCTION__<< ": " <<__VA_ARGS__<<std::endl; \
	}
	
#define WARN(...) \
	std::cerr<<__FUNCTION__<<": " <<__VA_ARGS__<<std::endl;
#define ERR(...) \
	{ \
		std::cerr<<__FUNCTION__<<": " <<__VA_ARGS__<<std::endl; \
		exit(1); \
	}
#endif
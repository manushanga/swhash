# Madura A.
# Checks output with the previous research
#
import os
import sys

ss=set()

if (len(sys.argv) < 3):
	print("Usage check_output <rafal's output> <madura's output>")
else:
	ffr = open(sys.argv[1])
	while True:
		line = ffr.readline()
		if line=='':
			break
		else:
			sline=line.split(';')
			if (len(sline) == 4):
				ss.add(line.split(';')[3])
	ffr.close()
	
	ffm = open(sys.argv[2])
	while True:
		line = ffm.readline()
		if line=='':
			break
		else:
			if not line in ss:
				print(line[:-1])
				
	ffm.close()
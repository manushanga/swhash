/*
 * Copyright 2007 Rafal Pokrzywa
 * 
 * This file is part of YeastsIdentification by Rafal Pokrzywa
 * 
 * YeastsIdentification is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * TandemRepeats is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TandemRepeats; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
package rpo.genomic.data;

/**
 * The <code>YeastSequence</code> class represents an yeast sequence. 
 * 
 * @author Rafal Pokrzywa
 */
public class YeastSequence {
	
    /** Character appended to the end of easch yeast sequence */
	public static final char SEQUENCE_SEPARATOR = '$';

	private String number;
	
	private String name;
	
	private String sequence;
	
	public YeastSequence(String number, String name, String sequence) {
		this.number = number;
		this.name = name;
		this.sequence = sequence;
	}
	
	public YeastSequence(String number, String sequence) {
		this(number, null, sequence);
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	public int length() {
		return sequence != null ? sequence.length() : 0;
	}

}

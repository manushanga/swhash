 
import sys
import matplotlib.pyplot as plt
import matplotlib
fori=open(sys.argv[1],'r')

dd={}

for line in fori.readlines():
  line=line.strip()
  a=len(line)
  if not a in dd:
    dd[a]=1
  else:
    dd[a]+=1
    
font = {'family' : 'Arial',
        'weight' : 'regular',
        'size'   : 12}

matplotlib.rc('font', **font)    

plt.figure(figsize=(10,5))
plt.bar(range(len(dd)), dd.values())
plt.xticks(range(len(dd)), list(dd.keys()))

plt.show()
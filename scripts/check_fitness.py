import sys
import re

import matplotlib.pyplot as plt

fori = open(sys.argv[1],'r')
picked=0

def longest(line):
  c=line[0]
  dd={}
  rep=1
  for a in line:
    if a == c:
      rep+=1
    else:
     
      if not c in dd:
        dd[c]=rep
      else:
        dd[c]=max(dd[c],rep)
      c=a
      
      rep=1
      
  return max(dd.values())
dd={}
allr=0
for line in fori.readlines():
  counts = {'a':0,'c':0,'g':0,'t':0}
  for a in line:
    if a in counts:
      counts[a] +=1
  acgt=counts['a']+counts['c']+counts['g']+counts['t'] 
  gc=counts['g'] + counts['c']
  gccontent = gc/acgt
  mtemp = 4*gc + 2*(acgt-gc)
  repeats=longest(line)
  allr+=1
  if (repeats <5 and gccontent > 0.4 and gccontent < 0.6 and (mtemp > 51 and mtemp <59 )):
    a=len(line)
    if not a in dd:
      dd[a]=1
    else:
      dd[a]+=1
    picked+=1
  
  
plt.figure(figsize=(5,3))
plt.bar(range(len(dd)), dd.values(),width=0.3,align='center')

plt.xticks(range(len(dd)), list(dd.keys()))

plt.show()

print(picked,allr,picked/allr)
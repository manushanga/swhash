import sys
import os
import random

if (len(sys.argv)<3):
	print("Usage: split_files.py <sequences> <where_to>")
else:
	ffm=open(sys.argv[1],"r")
	seqs = []
	line = ffm.readline()[:-1]
	seq = line
	while (line!=""):
		if line.startswith(">"):
			seqs.append((line, seq))
			seq = ""
		else:
			seq += line
		line = ffm.readline()[:-1]
	ffm.close()
	
	for i,j in seqs:
		ffx=open("%s/%s.txt"%(sys.argv[2],i.split(' ')[0].replace('>','')),'w')
		ffx.write(j)
		ffx.write('\n')
		ffx.close()

import sys
import os
import random

if (len(sys.argv)<3):
	print("Usage: make_tests.py <all_sequences> <max_size_per_file>")
else:
	ffm=open(sys.argv[1],"r")
	seqs = []
	line = ffm.readline()[:-1]
	seq = line
	while (line!=""):
		if line.startswith(">"):
			seqs.append((line, seq))
			seq = ""
		else:
			seq += line
		line = ffm.readline()[:-1]
	ffm.close()
	maxsize=int(sys.argv[2])
	minsize=10
	
	while (minsize<maxsize):
		pp=random.sample(seqs, minsize)
		ffx=open("sequences_%d.txt"%(minsize),'w')
		for i,j in pp:
			ffx.write(i)
			ffx.write('\n')
			ffx.write(j)
			ffx.write('\n')
		ffx.close()
		minsize*=2
	
#include "naive.h"
#include <iostream>
RadixComparator::RadixComparator(uint64_t column, uint8_t* input)
: col(column), in(input)
{
}
bool RadixComparator::operator()(const uint64_t& a, const uint64_t& b)
{
	return in[a+col] < in[b+col];
}

SASFunc* SASNaive::create()
{
	static SASNaive naive;
	return &naive;
}

SASNaive::SASNaive()
{
}
void SASNaive::RadixRecursive(uint8_t *input, uint64_t current, uint64_t low, uint64_t high, uint64_t *output)
{
	
	std::sort(output+low, output+high+1,RadixComparator(current,input));
		
	if (current < unique_max_length){
		
		//Alphabet::print_output(input,output,total);
		uint8_t sym=input[output[low]+current];
		uint8_t thissym;
		uint64_t skip_ends=low;
		uint64_t idx;
		DBG(2,low);
		uint64_t sublow=low,subhigh=low;
		
		if (sym==ALPHA_SEQ_END || sym==ALPHA_AMBIGUOUS) {
			while ( (sym==ALPHA_SEQ_END || sym==ALPHA_AMBIGUOUS) && skip_ends <= high){
				idx=output[skip_ends]+current;
				sym=input[idx];
				DBG(2,Alphabet::map[sym]);
				skip_ends++;
			}
			sublow=subhigh=skip_ends-1;
		} else {
			sym=input[output[skip_ends]+current];
		}
		
		for (uint64_t i=skip_ends;i<=high;i++)
		{
			idx=output[i]+current;
			thissym=input[idx];
			DBG(2,Alphabet::map[thissym]);
			if ( thissym != sym){
				subhigh = i-1;
				if (sublow < subhigh ){
					DBG(2,current<<":" <<Alphabet::map[sym]<<" "<<Alphabet::map[thissym]<<" "<<sublow<<" "<<subhigh);
					//Alphabet::print_output(input,output,total);
					RadixRecursive(input, current + 1, sublow, subhigh, output);
				}
				sublow = i;
				sym=thissym;
			}
			
		}
		
		if (sublow< high) {
			DBG(2,current<<"x:" <<Alphabet::map[sym]<<" "<<sublow<<" "<<high);
			RadixRecursive(input, current + 1, sublow, high, output);
		}
	}
}

void SASNaive::SASRun(void *input, void *output)
{
	uint8_t *in=(uint8_t*) input;
	uint64_t *out=(uint64_t*) output;
	for (uint64_t i=0;i<total;i++)
	{
		out[i]=i;
	}
	RadixRecursive(in, 0, 0, total-1, out);
	/*for (int i=0;i<total;i++)
	{
		
		printf("%lu\n",out[i]);
	}*/
}


void SASNaive::SASNextUniqueSequence(uint8_t* input, uint64_t* output, SASUniqueSequenceIterator* iterator, SASUniqueSequence* sequence)
{
	sequence->start = (uint64_t)-1UL;
	if (input[output[iterator->index]] == ALPHA_SEQ_END ){
		// add 2 to skip the starting $ at the row above starting row
		iterator->index += 1;
	} else if (iterator->index < total-1) {
		//printf("%lu\n",sequence->start);
		// we assume unique_max_length < any sequence length
		uint64_t upperidx=output[iterator->index-1];
		uint64_t idx=output[iterator->index];
		uint64_t belowidx=output[iterator->index+1];
		
		uint64_t upperLCP=0;
		uint64_t belowLCP=0;
		
		uint64_t i;
		uint64_t maxlen=unique_max_length;
		for (i=0;i<unique_max_length;i++){
			
			// we skip here to avoid from overly complex machinery for the corner
			// cases, 99% of unqiues can be taken from the other methods 
			
			if (i+upperidx >= total || i+idx >= total || i+belowidx >= total)
			{
				iterator->index++;
				return;
			}
			if ( (input[i+upperidx] == ALPHA_AMBIGUOUS ) ||
				 (input[i+idx] == ALPHA_AMBIGUOUS ) || 
				 (input[i+belowidx] == ALPHA_AMBIGUOUS )){
				maxlen=i;	
				break;
			} else if (input[i+idx] == ALPHA_SEQ_END ){
				maxlen=i;
				break;
			}
			
			
		}
		
		
		bool loopb1=false,loopb2=false,end1=false,end2=false;
		for (i=0;i<maxlen;i++){

			
			if (input[i+idx] != input[i+upperidx] || input[i+upperidx] == ALPHA_SEQ_END){
				upperLCP=i;
				loopb1=true;
				break;
			}
		}
		for (i=0;i<maxlen;i++){
			
			
			if (input[i+idx] != input[i+belowidx] || input[i+belowidx] == ALPHA_SEQ_END){
				belowLCP=i;
				loopb2=true;
				break;
			}
		}
		
		
		
		
		uint64_t lcp =upperLCP > belowLCP ? upperLCP : belowLCP ;
		if (loopb1 && loopb2 ) {
			if (lcp == 0){
				sequence->start = idx;
				sequence->end = sequence->start + maxlen;
				
			} else {
				//std::cout<<upperLCP <<" "<< belowLCP<<" " <<lcp <<" "<< iterator->index<<std::endl;
				sequence->start = idx;
				sequence->end = sequence->start + lcp ;
				
			}
		} 
		iterator->index++;
		
	}
}

SASNaive::~SASNaive()
{

}

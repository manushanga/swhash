# README #

## Building ##
    mkdir build
    cd build
    cmake .. -DPROGRAM="SWHASH,BITMAP,SAS" # you can use any set of SWHASH,BITMAP and SAS
    make

## Running ##
`sas` and `swhash` are executables for testing the Suffix Array Sorting and Sliding Window Hash algorithms in order. `bitmap` executable can be used for comparison of outputs. Get more information by running these programs without any switch.

## License ##

All original work by me (see thesis.pdf) are released under GNU GPL v2.0. Other projects that this work depend on (namely Primer3) are licensed under their original license. The sources have not been changed by me, the distribution is done only for purposes of easier deployment. 
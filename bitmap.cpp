#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <iostream>


uint64_t sum=0L,matches=0L;
int main (int argc, char **argv)
{
	FILE *f1=fopen(argv[1],"r");
	FILE *f2=fopen(argv[2],"r");
	while (!feof(f1) && !feof(f2))
	{
		uint64_t total=0;
		fread(&total, sizeof(uint64_t), 1, f1);
		//printf("%lu\n ",total );
		uint8_t *data1=(uint8_t *)malloc(total/8+1);
		fread(data1,1,total/8+1,f1);
		uint8_t *data2=(uint8_t *)malloc(total/8+1);
		total=0;
		fread(&total, sizeof(uint64_t), 1, f2);
		//printf("%lu\n ",total );
		
		fread(data2,1,total/8+1,f2);

		for (uint64_t i=0;i<total;i++)
		{
			if (data1[i/8] >> (i & 7) ==0x01U ) {
				sum++;
				if (data1[i/8] >> (i & 7) == data2[i/8] >> (i & 7)) 
				{
					matches++;
				}
			}
		}
		free(data1);
		free(data2);
	}
	std::cout<<"All recognized:"<<sum<<" SWHASH:"<<matches<<" "
	         <<double(sum-matches)/double(matches)<<std::endl;
	fclose(f1);
	fclose(f2);
}
/*
 * Copyright 2007 Rafal Pokrzywa
 * 
 * This file is part of YeastsIdentification by Rafal Pokrzywa
 * 
 * YeastsIdentification is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * TandemRepeats is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with TandemRepeats; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
package rpo.genomic.algorithm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;

import rpo.genomic.data.UniqueSequence;
import rpo.genomic.data.YeastSequence;

/**
 * The <code>UniqueYeastSpecies</code> class allows to indentify yeast species. 
 * UniqueYeastSpecies is based on the Burrows-Wheeler Transform.
 * 
 * @author Rafal Pokrzywa
 */
public class UniqueYeastSpecies {

    /** Character appended to the end of input string required by the BWT */
	private static final char GLOBAL_SEPARATOR = '#';

	private ArrayList yeastSequences;
	
	private String source;
	
	private int[] seqLengths;

	private int maxLength = 0;

	public UniqueYeastSpecies(String source) {
		this.source = source + GLOBAL_SEPARATOR;
	}
	
	public UniqueYeastSpecies(ArrayList yeastSequences) {
		this.yeastSequences = yeastSequences;
	}
	
	public Collection searchUniqueSequences() {
		if (source == null) {
			seqLengths = new int[yeastSequences.size()];
			StringBuffer buffer = new StringBuffer();
			for (int i = 0; i < yeastSequences.size(); i++) {
				YeastSequence yeastSequence = (YeastSequence) yeastSequences.get(i);
				buffer.append(yeastSequence.getSequence());
				if (maxLength < yeastSequence.length())
					maxLength = yeastSequence.length();
				seqLengths[i] = yeastSequence.length();
				if (i > 0)
					seqLengths[i] += seqLengths[i - 1];
			}
			source = buffer.toString() + GLOBAL_SEPARATOR;
		}
		int[] charactersCount = countCharacters();
		int[] suffixArray = createSuffixArray();
		char[] burrowsWheeler = createBurrowsWheeler(suffixArray);
		int[] rankNext = createRankNext(charactersCount, burrowsWheeler);
		int[] longestCommonPrefixes = createLongestCommonPrefix(suffixArray, rankNext);
		
		return findUniqueSequences(suffixArray, longestCommonPrefixes);
	}
	
	private int[] countCharacters() {
		int[] charactersCount = new int[128];
		Arrays.fill(charactersCount, 0);
		for (int i = 0; i < source.length(); i++) {
			charactersCount[source.charAt(i)]++;
		}
		int sum = 0;
		for (int i = 0; i < charactersCount.length; i++) {
			if (charactersCount[i] > 0) {
				int count = charactersCount[i];
				charactersCount[i] -= charactersCount[i];
				charactersCount[i] += sum;
				sum += count;
			}
		}
		return charactersCount;
	}
	
	private int[] createSuffixArray() {
    	Integer[] sortIndices = new Integer[source.length()];
        for (int i = 0; i < sortIndices.length; i++) {
        	sortIndices[i] = new Integer(i);
        }
        
        Arrays.sort(sortIndices, new Comparator() {
            public int compare(Object o1, Object o2) {
                Integer index1 = (Integer) o1;
                Integer index2 = (Integer) o2;
                return source.substring(index1.intValue()).compareTo(
                		source.substring(index2.intValue()));
            }
        });

        int[] suffixArray = new int[sortIndices.length];
        for (int i = 0; i < suffixArray.length; i++) {
			suffixArray[i] = sortIndices[i].intValue();
		}
        return suffixArray;
	}
	
	private char[] createBurrowsWheeler(int[] suffixArray) {
		char[] burrowsWheeler = new char[suffixArray.length];
		for (int i = 0; i < burrowsWheeler.length; i++) {
        	if (suffixArray[i] == 0) {
        		burrowsWheeler[i] = GLOBAL_SEPARATOR;
        	} else {
        		burrowsWheeler[i] = source.charAt(suffixArray[i] - 1);
        	}
		}
		return burrowsWheeler;
	}
	
	/**
	 * Calculates vector T[0, n - 1] for the Burrows-Wheeler Transform
	 * @param charactersCount
	 * @param burrowsWheeler
	 * @return Vector T[0, n - 1] for the Burrows-Wheeler Transform
	 */
	private int[] createRankNext(int[] charactersCount, char[] burrowsWheeler) {
		int[] rankNext = new int[burrowsWheeler.length];
		for (int i = 0; i < rankNext.length; i++) {
			char character = burrowsWheeler[i];
			int j = charactersCount[character]++;
			rankNext[j] = i;
		}
		return rankNext;
	}
	
	private int[] createLongestCommonPrefix(int[] suffixArray, int[] rankNext) {
		int n = suffixArray.length;
		int k = rankNext[0];
		int h = 0;
		for (int i = 0; i < n; i++) {
			int next = rankNext[k];
			if (k == 0) {
				rankNext[k] = -1;
			} else {
				int j = suffixArray[k - 1];
				while (i + h < n && j + h < n && source.charAt(i + h) == source.charAt(j + h)) {
					h++;
				}
				rankNext[k] = h;
			}
			if (h > 0)
				h--;
			k = next;
		}
		return rankNext;
	}
	
	private Collection findUniqueSequences(int[] suffixArray, int[] lcp) {
		int n = suffixArray.length;
		ArrayList uniqueSequences = new ArrayList();
		for (int i = 1; i < n - 1; i++) {
			int sequenceIndex = getSequenceIndex(suffixArray[i]);
            int offset = 1;
            while (i + offset < n && sequenceIndex == getSequenceIndex(suffixArray[i + offset])) {
                offset++;
            }
            if (i + offset < n) {
                int unique = Math.max(lcp[i], lcp[i + offset]);
                if (suffixArray[i] + unique + 1 < n 
                        && source.substring(suffixArray[i], suffixArray[i] + unique + 1)
                        	.indexOf(YeastSequence.SEQUENCE_SEPARATOR) == -1) {
                    
                	if (sequenceIndex > yeastSequences.size()) {
                		throw new RuntimeException("There is no sequence of index " + sequenceIndex);
                	} else {
                		YeastSequence yeastSequence = (YeastSequence) yeastSequences.get(sequenceIndex - 1);
                		int begin = sequenceIndex < 2 ? 0 : seqLengths[sequenceIndex - 2];
                		int start = suffixArray[i] - begin + 1;
                    	UniqueSequence uniqueSequence = new UniqueSequence(yeastSequence, start, unique + 1);
                    	uniqueSequences.add(uniqueSequence);
                	}
                }
            }
		}
		if (suffixArray[n - 1] + lcp[n - 1] + 1 < n) {
			int sequenceIndex = getSequenceIndex(suffixArray[n - 1]);
    		YeastSequence yeastSequence = (YeastSequence) yeastSequences.get(sequenceIndex - 1);
    		int begin = sequenceIndex < 2 ? 0 : seqLengths[sequenceIndex - 2];
    		int start = suffixArray[n - 1] - begin + 1;
        	UniqueSequence uniqueSequence = new UniqueSequence(yeastSequence, start, lcp[n - 1] + 1);
        	uniqueSequences.add(uniqueSequence);
		}
		
		return uniqueSequences;
	}
	
	private int getSequenceIndex(int suffixIndex) {
		int sequenceNumber = suffixIndex / maxLength;
		while (sequenceNumber < seqLengths.length && suffixIndex >= seqLengths[sequenceNumber]) {
			sequenceNumber++;
		}
		return sequenceNumber + 1;
	};
	
	private static void calculateAvgLegthAtPosition(Collection uniqueSequences) {
    	HashMap avgLegthAtPosition = new HashMap();
    	int maxPosition = -1;
        for (Iterator iter = uniqueSequences.iterator(); iter.hasNext();) {
            UniqueSequence uniqueSequence = (UniqueSequence) iter.next();
            if (avgLegthAtPosition.containsKey(new Integer(uniqueSequence.getStart()))) {
            	ArrayList lengths = (ArrayList) avgLegthAtPosition
            		.get(new Integer(uniqueSequence.getStart()));
            	lengths.add(new Integer(uniqueSequence.getLength()));
            	avgLegthAtPosition.put(new Integer(uniqueSequence.getStart()), lengths);
            } else {
            	ArrayList lengths = new ArrayList();
            	lengths.add(new Integer(uniqueSequence.getLength()));
            	avgLegthAtPosition.put(new Integer(uniqueSequence.getStart()), lengths);
            }
            if (uniqueSequence.getStart() > maxPosition)
            	maxPosition = uniqueSequence.getStart();
        }
        
        for (int i = 0; i <= maxPosition; i++) {
            if (avgLegthAtPosition.containsKey(new Integer(i))) {
            	ArrayList lengths = (ArrayList) avgLegthAtPosition.get(new Integer(i));
            	int size = lengths.size();
            	double sum = 0;
            	for (Iterator iter = lengths.iterator(); iter.hasNext();) {
					Integer length = (Integer) iter.next();
					sum += length.intValue();
				}
            	System.out.println(sum / size);
            } else {
            	System.out.println("0");
            }
		}
	}

	private static void calculateUniquesAtPosition(Collection uniqueSequences, int limit) {
    	HashMap avgLegthAtPosition = new HashMap();
    	int maxPosition = -1;
        for (Iterator iter = uniqueSequences.iterator(); iter.hasNext();) {
            UniqueSequence uniqueSequence = (UniqueSequence) iter.next();
            if (uniqueSequence.getLength() > limit)
            	continue;
            if (avgLegthAtPosition.containsKey(new Integer(uniqueSequence.getStart()))) {
            	ArrayList lengths = (ArrayList) avgLegthAtPosition
            		.get(new Integer(uniqueSequence.getStart()));
            	lengths.add(new Integer(uniqueSequence.getLength()));
            	avgLegthAtPosition.put(new Integer(uniqueSequence.getStart()), lengths);
            } else {
            	ArrayList lengths = new ArrayList();
            	lengths.add(new Integer(uniqueSequence.getLength()));
            	avgLegthAtPosition.put(new Integer(uniqueSequence.getStart()), lengths);
            }
            if (uniqueSequence.getStart() > maxPosition)
            	maxPosition = uniqueSequence.getStart();
        }
        
        for (int i = 0; i <= maxPosition; i++) {
            if (avgLegthAtPosition.containsKey(new Integer(i))) {
            	ArrayList lengths = (ArrayList) avgLegthAtPosition.get(new Integer(i));
            	System.out.println(lengths.size());
            } else {
            	System.out.println("0");
            }
		}
	}

    /**
     * The main method to start YeastsIdentification application.
     * @param args
     */
    public static void main(String[] args) {
    	String inputFolder = FileManager.SEQUENCES_FOLDER;
    	if (args.length > 0)
    		inputFolder = args[0];

    	int limit = 6;
    	if (args.length > 1) {
    		try {
				limit = Integer.parseInt(args[1]);
			} catch (NumberFormatException e) {
				limit = 6;
			}
    	}
    	
    	String outputFile = "report.txt";
    	if (args.length > 2)
    		outputFile = args[2];

    	ArrayList yeastSequences = FileManager.loadFolderSequences(inputFolder);
		System.out.println(yeastSequences.size() + " sequence(s) to analize");	

		if (yeastSequences.size() > 0) {
			UniqueYeastSpecies uniqueYeastSpecies = new UniqueYeastSpecies(yeastSequences);
			long start = System.currentTimeMillis();
			Collection uniqueSequences = uniqueYeastSpecies.searchUniqueSequences();
			long end = System.currentTimeMillis();
			System.out.println("Time: "+ (end-start));
			System.out.println(uniqueSequences.size() + " all unique sequences found");	
			int identified = FileManager.saveFinalReport(outputFile, uniqueSequences, yeastSequences.size(), limit);
	        System.out.println(identified + " of " + yeastSequences.size() + " yeast species where identified");
	        
//	        calculateAvgLegthAtPosition(uniqueSequences);
//	        calculateUniquesAtPosition(uniqueSequences, 10);
		}
	}
		
}

